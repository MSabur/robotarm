/*
 * main.cpp
 *
 *  Created on: Feb 24, 2017
 *      Author: maurice
 * 		Email: 	m.rozema2@student.han.nl
 */

#include <thread>

#include "ObjectDetector.hpp"
#include "SerialParser.hpp"
#include "ros/ros.h"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "opencv_bp2");
    ros::start();
    //ros::Rate loop_rate(10);

    if (argc > 1)
        ObjectDetector object_detector(argv[1]);
    else
        ObjectDetector object_detector;  

    return 0;
}