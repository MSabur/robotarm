/*
 * Serial_Parser.cpp
 *
 *  Created on: Feb 24, 2017
 *      Author: maurice
 * 		Email: 	m.rozema2@student.han.nl
 */

#include "SerialParser.hpp"

SerialParser::SerialParser()
{
    // TODO Auto-generated constructor stub
}

SerialParser::~SerialParser()
{
    // TODO Auto-generated destructor stub
}

void SerialParser::waitForUserInput(
    std::vector<std::pair<object_shapes, object_colors>>& object_specifications)
{
    std::string input_parameters = "", object_shape, object_color;
    std::cout << "Enter the specifications like [vorm][whitespace][kleur]:"
              << std::endl;
    std::getline(std::cin, input_parameters);

    appendString(input_parameters, object_shape, object_color);

    inputCheck(object_specifications, object_shape, object_color);

    waitForUserInput(object_specifications);
}

void SerialParser::BashInput(
    const std::string& pathToBashFile,
    std::vector<std::pair<object_shapes, object_colors>>& object_specifications)
{
    std::ifstream myfile;
    myfile.open(pathToBashFile);
    if (!myfile.is_open())
        std::cout << "Couldn't open file." << std::endl;

    std::string input_parameters = "", object_shape, object_color;
    while (std::getline(myfile, input_parameters)) {
        //        if (!std::string(selection).compare("#"))
        //            std::cout << "Lol Nee" << std::endl;
        appendString(input_parameters, object_shape, object_color);
        inputCheck(object_specifications, object_shape, object_color);
    }
}

void SerialParser::appendString(const std::string& input_param,
                                std::string& aShape, std::string& aColor)
{
    std::istringstream input_parameters_stream(input_param);
    if (input_param.find("halve cirkel") != std::string::npos) {
        std::string tmp_string;
        input_parameters_stream >> aShape >> tmp_string >> aColor;
        aShape.append(" " + tmp_string);
    } else
        input_parameters_stream >> aShape >> aColor;
}

void SerialParser::inputCheck(
    std::vector<std::pair<object_shapes, object_colors>>& object_specifications,
    const std::string& object_shape, const std::string& object_color)
{
    std::pair<object_shapes, object_colors> tmpPair(shape_not_initialized,
                                                    color_not_initialized);

    inputShape(tmpPair, object_shape);
    inputColor(tmpPair, object_color);

    if (tmpPair.first != shape_not_initialized &&
        tmpPair.second != color_not_initialized)
        object_specifications.push_back(tmpPair);
    else {
        std::cout << "Vormen:" << std::endl;
        std::cout << "1. Cirkel" << std::endl;
        std::cout << "2. Halve cirkel" << std::endl;
        std::cout << "3. Vierkant" << std::endl;
        std::cout << "4. Rechthoek" << std::endl;
        std::cout << "5. Driehoek" << std::endl << std::endl;
        std::cout << "Kleuren:" << std::endl;
        std::cout << "1. Rood" << std::endl;
        std::cout << "2. Groen" << std::endl;
        std::cout << "3. Blauw" << std::endl;
        std::cout << "4. Geel" << std::endl;
        std::cout << "5. Zwart" << std::endl;
        std::cout << "6. wit" << std::endl << std::endl;
    }
}

void SerialParser::inputShape(std::pair<object_shapes, object_colors>& tmpPair,
                              const std::string& object_shape)
{
    if (!object_shape.compare("cirkel"))
        tmpPair.first = cirkel;
    else if (!object_shape.compare("halve cirkel"))
        tmpPair.first = halve_cirkel;
    else if (!object_shape.compare("vierkant"))
        tmpPair.first = vierkant;
    else if (!object_shape.compare("rechthoek"))
        tmpPair.first = rechthoek;
    else if (!object_shape.compare("driehoek"))
        tmpPair.first = driehoek;
}

void SerialParser::inputColor(std::pair<object_shapes, object_colors>& tmpPair,
                              const std::string& object_color)
{
    if (!object_color.compare("rood"))
        tmpPair.second = rood;
    else if (!object_color.compare("groen"))
        tmpPair.second = groen;
    else if (!object_color.compare("blauw"))
        tmpPair.second = blauw;
    else if (!object_color.compare("geel"))
        tmpPair.second = geel;
    else if (!object_color.compare("zwart"))
        tmpPair.second = zwart;
    else if (!object_color.compare("wit"))
        tmpPair.second = wit;
}
