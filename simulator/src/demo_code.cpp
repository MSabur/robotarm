#include <ros/ros.h>
#include <sstream>
#include <thread>

#include "std_msgs/String.h"

int main(int argc, char** argv)
{
  std::vector<std::string> demo_positions = {
    "#0P500S1000#5P500S1000#4P1500S1000"
    "#1P900S700#2P1411S1000#3P2055S1000",
    "#5P2500S1000",
    "#0P2400S1000#1P1472S700#2P904S1000#3P1833S1000T1500",
    "#0P1450S1000#1P1186S700#2P700S1000#3P1611S1000T1500",
    "#5P500S1000",
  };

  ros::init(argc, argv, "demo_code");
  std::this_thread::sleep_for(std::chrono::seconds(1));
  ros::NodeHandle nh;
  ros::Publisher position_publisher = nh.advertise<std_msgs::String>("SSC32Commands", 1000);
  std::this_thread::sleep_for(std::chrono::seconds(4));
  std_msgs::String msg;

  if(ros::ok()) {
    for(unsigned int count = 0; count < demo_positions.size(); count++) {

      std::cout << "Count: " << count << std::endl;
      std::cout << "Going to send: " << demo_positions[count] << std::endl;
      msg.data = demo_positions[count];
      position_publisher.publish(msg);
      ros::spinOnce();
      std::this_thread::sleep_for(std::chrono::seconds(2));
    }
  }
  return 0;
}
