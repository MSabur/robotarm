#include <signal.h>

#include "ActionManager.hpp"

void spinThread() {
    ros::spin();
}

void exitHandler(int s) {
    ROS_INFO("\nExiting...\n");
    exit(1);
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "ActionManager");

    ActionManager manager("LowInterface");

    boost::thread spin_thread(&spinThread);
    signal(SIGINT, exitHandler);

    manager.waitForServer();
    manager.spin();
    spin_thread.join();

}