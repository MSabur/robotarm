/*
 * Serial_Parser.hpp
 *
 *  Created on: Feb 24, 2017
 *      Author: maurice
 * 		Email: 	m.rozema2@student.han.nl
 */

#ifndef SERIALPARSER_HPP_
#define SERIALPARSER_HPP_

#include <boost/algorithm/string.hpp>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "Enums.hpp"

class SerialParser
{
  public:
    /**
     * Constructor
     **/
    SerialParser();

    /**
     * Destructor
     **/
    virtual ~SerialParser();

    /**
     * Waits for user input on the console.
     * Because this is a multi-threaded application a reference is passed
     * from a vector which object_shapes and object_colors.
     * If user input is available, it will check if it is a valid command.
     * If it is valid if will push the command at the back of the vector.
     * The vector will be used in the statemachine of ObjectDetector
     *
     * @param object_specifications	Vector which pairs of object_shapes and
     *object_colors.
     *
     * @see programState()
     **/
    void waitForUserInput(std::vector<std::pair<object_shapes, object_colors>>&
                              object_specifications);

    /**
     * Almost the same as waitForUserInput(), but this will read a batch file.
     * @see waitForUserInput()
     **/
    void BashInput(const std::string& pathToBashFile,
                   std::vector<std::pair<object_shapes, object_colors>>&
                       object_specifications);

  private:
    /**
     * This will append the command on the console to the object_shape string
     * This string will be used in further functions.
     *
     * @param input_param	The command on the console.
     * @param aShape		The string of the shape.
     * @param aColor		The string of the color.
     **/
    void appendString(const std::string& input_param, std::string& aShape,
                      std::string& aColor);

    /**
     * This function checks if the command is valid.
     * If it is valid it will push it at the back of the vector.
     *
     * @param object_specifications		Reference to the vector as
     *stated
     *in
     *ObjectDetector.
     **/
    void inputCheck(std::vector<std::pair<object_shapes, object_colors>>&
                        object_specifications,
                    const std::string& object_shape,
                    const std::string& object_color);

    /**
     * This function checks if the shape is valid.
     * If it is valid is will set the first value on the pair.
     *
     * @param tmpPair		Pair to set the value.
     * @param object_shape	The shape in the command, given by the user.
     **/
    void inputShape(std::pair<object_shapes, object_colors>& tmpPair,
                    const std::string& object_shape);

    /**
     * This function checks if the color is valid.
     * If it is valid is will set the second value on the pair.
     *
     * @param tmpPair		Pair to set the value.
     * @param object_color	The color in the command, given by the user.
     **/
    void inputColor(std::pair<object_shapes, object_colors>& tmpPair,
                    const std::string& object_color);
};

#endif /* SERIALPARSER_HPP_ */
