#include "HighInterface.hpp"

#include <ros/ros.h>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

HighInterface::HighInterface() {
    movePub = nh.advertise<robotarm::Joints>("MovementsQue", 20);
    instaPub = nh.advertise<robotarm::Insta>("InstaMsgs", 2);
}

void HighInterface::publishMove(robotarm::Joints& aMove) {
    movePub.publish(aMove);
}

void HighInterface::publishMove() {
    movePub.publish(move);
}

void HighInterface::publishInstaMsg() {
    instaPub.publish(instaMsg);
}

void HighInterface::waitForSubs() {
    ros::Rate r(2);
    int loops = 0;
    while (!movePub.getNumSubscribers()) {
        ROS_INFO_ONCE("Waiting for subscribers on the 'MovementsQue' topic.");
                cout << "\r" << flush;

        for (int i = 0; i < loops; i++) {
            cout << ". ";

        }
        loops++;
        r.sleep();
    }

    if(loops) cout << "Done!" << endl;
}

void HighInterface::printJoints() {

    const string uc = "UNSET";
    cout << "Base:............" << (move.base == move.UNSET ? uc : to_string(move.base)) << endl;
    cout << "Shoulder:........" << (move.shoulder == move.UNSET ? uc : to_string(move.shoulder)) << endl;
    cout << "Elbow:..........." << (move.elbow == move.UNSET ? uc : to_string(move.elbow)) << endl;
    cout << "Wrist:..........." << (move.wrist == move.UNSET ? uc : to_string(move.wrist)) << endl;
    cout << "Wrist Rotation:.." << (move.wristRotation == move.UNSET ? uc : to_string(move.wristRotation)) << endl;
    cout << "Gripper:........." << (move.gripper == move.UNSET ? uc : to_string(move.gripper)) << endl;
    cout << "Completion Time:." << (move.time == 0 ? uc : to_string(move.time)) << endl;
    cout << endl;
}

void HighInterface::printHelp() {
    cout << "\033[1;34m" << "Valid Commands:" << "\033[0m" << endl;
    cout << "\tstop          | Stop immediately." << endl;
    cout << "\tpark          | Park stance." << endl;
    cout << "\tready         | Ready stance." << endl;
    cout << "\tstraight-up   | Stand-up stance." << endl;
    cout << "With value:" << endl;
    cout << "\tbase          | Int between -90 and 90 " << endl;
    cout << "\tshoulder      | Int between -30 and 90 " << endl;
    cout << "\telbow         | Int between 0 and 135" << endl;
    cout << "\twrist         | Int between 90 and -90" << endl;
    cout << "\twristRotation | Int between -90 and 90 " << endl;
    cout << "\tgripper       | Int between -90 and 90 " << endl;
    cout << "\ttime          | Desired completion time in milliseconds as integer." << endl;
    cout << endl;
    cout << "Example 1: stop" << endl;
    cout << "Example 2: park time=2000" << endl;
    cout << "Example 2: base=45 elbow=100" << endl;
}

bool HighInterface::userValidate() {

    printJoints();
    cout << "Publish movement? (y/n)\n" << endl;
    ;
    string response;
    getline(cin, response);
    boost::to_upper(response);
    if (response == "Y" || response == "YES") return true;
    if (response == "N" || response == "NO") return false;
    userValidate();
    return false;
}

void HighInterface::setDefaults() {
    move.base = move.UNSET;
    move.shoulder = move.UNSET;
    move.elbow = move.UNSET;
    move.wrist = move.UNSET;
    move.wristRotation = move.UNSET;
    move.gripper = move.UNSET;
    move.time = 0;
}

bool HighInterface::setStance(const string& value) {
    if (value == "park") {
        move.base = 0;
        move.shoulder = -30;
        move.elbow = 135;
        move.wrist = -90;
        move.wristRotation = 0;
        move.gripper = -90;
        move.time = 0;
        return true;
    }
    if (value == "ready") {
        move.base = 0;
        move.shoulder = -30;
        move.elbow = 115;
        move.wrist = -20;
        move.wristRotation = 0;
        move.gripper = -90;
        move.time = 0;
        return true;
    }
    if (value == "straight-up" || value == "straightup") {
        move.base = 0;
        move.shoulder = 0;
        move.elbow = 0;
        move.wrist = 0;
        move.wristRotation = 0;
        move.gripper = -90;
        move.time = 0;
        return true;
    }
    return false;
}

bool HighInterface::getUserInput(istream& stream) {
    size_t pos = 0;
    bool isCLI = false;
    if (&stream == &cin) {
        isCLI = true;
    }

    setDefaults();
    args.clear();
    if (isCLI) {
        ROS_INFO_ONCE("\n\nType 'help' for information");
        cout << "\033[1;34m" << "\nCreate new movement:\n" << "\033[0m";
    }

    string input;
    getline(stream, input);
    if (isCLI) cout << endl;

    if ((pos = input.find_first_of("#")) != string::npos) {
        input.erase(pos);
    }

    boost::algorithm::trim_left(input);
    boost::algorithm::trim_right(input);
    boost::to_lower(input);

    if (input.empty()) {
        return false;
    }

    if (!isCLI) {
        ROS_INFO_STREAM("From file: " << input);
    }

    if (isCLI && input == "help") {
        printHelp();
        return false;
    }
    if (input == "exit") {
        exit = true;
        return false;
    }

    while ((pos = input.find_first_of(" \t")) != string::npos) {
        args.push_back(input.substr(0, pos));
        input.erase(0, pos + 1);
    }

    args.push_back(input);
    return true;

}

bool HighInterface::createInstaMsg() {
    for (string arg : args) {

        if (arg == "stop" || arg == "clear") {
            instaMsg.memo = arg;
            return true;
        }
    }
    return false;
}

bool HighInterface::createMove(bool withValidation) {
    size_t pos = 0;

    for (string arg : args) {
        if (setStance(arg));
        else if ((pos = arg.find_first_of('=')) != string::npos) {
            string key = arg.substr(0, pos);
            string valueStr = arg.substr(pos + 1);
            int16_t value;
            try {
                value = boost::lexical_cast<int>(valueStr);
            } catch (boost::bad_lexical_cast& e) {
                ROS_WARN_STREAM("Invalid integer value: " << valueStr);
                cout << e.what() << endl;
                return false;
            }

            if (key == "time") {
                if (value < 0 || value > move.TIME_MAX) {
                    ROS_WARN_STREAM("Time value should be less then " << move.TIME_MAX);
                    return false;
                }
                move.time = value;
            } else if (key == "base") {
                if (value < move.BASE_MIN || value > move.BASE_MAX) {
                    ROS_WARN_STREAM("Base value should be between " << move.BASE_MIN<< " and " << move.BASE_MAX);
                    return false;
                }
                move.base = value;
            } else if (key == "shoulder") {
                if (value < move.SHOULDER_MIN || value > move.SHOULDER_MAX) {
                    ROS_WARN_STREAM("Shoulder value should be between -30 and 90.");
                    return false;
                }
                move.shoulder = value;
            } else if (key == "elbow") {
                if (value < move.ELBOW_MIN || value > move.ELBOW_MAX) {
                    ROS_WARN_STREAM("Elbow value should be between 0 and 135.");
                    return false;
                }
                move.elbow = value;
            } else if (key == "gripper") {
                if (value < move.GRIPPER_MIN || value > move.GRIPPER_MAX) {
                    ROS_WARN_STREAM("Gripper value should be between -90 and 90.");
                    return false;
                }
                move.gripper = value;
            } else if (key == "wrist") {
                if (value < move.WRIST_MIN || value > move.WRIST_MAX) {
                    ROS_WARN_STREAM("Wrist value should be between -90 and 90.");
                    return false;
                }
                move.wrist = value;
            } else if (key == "wristrotation") {
                if (value < move.WRISTROTATION_MIN || value > move.WRISTROTATION_MAX) {
                    ROS_WARN_STREAM("Wrist rotation value should be between -90 and 90.");
                    return false;
                }
                move.wristRotation = value;
            } else {
                ROS_WARN_STREAM("Unknown argument: " << arg);
                return false;
            }
        } else {
            ROS_WARN_STREAM("Unknown argument: " << arg);
            return false;
        }
    }
    if (withValidation) {
        return userValidate();
    }
    return true;
}

bool HighInterface::quit() {
    return exit;
}
