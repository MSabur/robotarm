/*
 * Enums.hpp
 *
 *  Created on: Mar 15, 2017
 *      Author: maurice
 * 		Email: 	m.rozema2@student.han.nl
 */

#ifndef ENUMS_HPP_
#define ENUMS_HPP_

/**
 * Enum for the possible states
 */
enum program_states {
	DETECTCIRCLE,
    DETECTING,
    TRESHHOLD,
    WAIT_FOR_INPUT,
    DRAWSHAPES,
    CLEAN_ALL,
    CONTOUR_TO_TEXT,
	CALCULATE_DIFFERENCE,
	SEND_OBJECT
};

/**
 * Enum for the possible shapes
 */
enum object_shapes {
    cirkel,
    halve_cirkel,
    vierkant,
    rechthoek,
    driehoek,
    shape_not_initialized
};

/**
 * Enum for the possible colors
 */
enum object_colors {
    rood,
    groen,
    blauw,
    geel,
    zwart,
    wit,
    color_not_initialized
};

#endif /* ENUMS_HPP_ */
