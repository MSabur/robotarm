#ifndef KINEMATICS_HPP
#define KINEMATICS_HPP

#include <robotarm/Joints.h>
#include "Matrix.hpp"


#define SHOULDER_ELBOW_MM 146.05
#define ELBOW_WRIST_MM 187.325
#define WRIST_GRIPPER_MM 85.725


using std::cout;
using std::endl;
using std::cos;
using std::sin;

const double pi = std::acos(-1);

//#define SIN_DEG(x) std::sin((x) / (180 / pi))
//#define COS_DEG(x) std::cos((x) / (180 / pi))

#define DEG_TO_RAD(x) ((x) * (pi/180.00))
#define RAD_TO_DEG(x) ((x) * (180.00/pi))

struct Point {
    double x;
    double y;
    double z;
};


class Kinematics {
    
public:
    Kinematics();
    bool inverse(const Point& p, robotarm::Joints& joints);
    bool inverseWithCosineRule(const Point& goal, robotarm::Joints& joints, double grabAngle = 0);
    Matrix<2, 1, double> forward( double a1, double a2, double a3,
                                            double x = 0, double y = 0, 
                                            double l1 = SHOULDER_ELBOW_MM, 
                                            double l2 = ELBOW_WRIST_MM,
                                            double l3 = WRIST_GRIPPER_MM    );
            
    virtual ~Kinematics();
private:
    Matrix<2, 3, double> getJacobian(double a0, double a1, double a2);
    bool check(robotarm::Joints& joints);

};

#endif /* KINEMATICS_HPP */

