#include <initializer_list>
#include <cstdint>
#include <iostream>
#include <array>
#include <cassert>

#ifndef MATRIX_H
#define MATRIX_H

//#define OSTREAM_INDENTED
#ifdef OSTREAM_INDENTED
#include <algorithm>
#endif

template<std::size_t H, std::size_t  W, typename T = int>
class Matrix {
public:
    static_assert(std::is_default_constructible<T>::value, "Data Structure requires default-constructible elements");

    Matrix(){};
    explicit Matrix(T value);
    Matrix(std::initializer_list<T>& il);
//    Matrix(std::initializer_list<std::initializer_list<T>>& il);
    Matrix(const std::array<T, W*H>& array);
    Matrix<H, W, T>& operator=(const Matrix<H, W, T>& mat);
    Matrix<H, W, T>& operator*=(T scalar);
    Matrix<H, W, T>& operator/=(T scalar);
    template <std::size_t W2>
    Matrix<H, W2, T> operator*(const Matrix<W, W2, T>& mat);
    
    Matrix<H, W, T> identity() const;
    Matrix<W, H, T> transpose() const;
    Matrix<H, W, T> inverse() const;
    inline T& at(std::size_t row, std::size_t col){return data[W * row + col];};
    bool approxEqual(const Matrix<H, W, T>& mat, const T maxDiff) const;
    void swapRows(std::size_t a, std::size_t b);
    inline std::size_t getWidth() { return W; }
    inline std::size_t getHeight() { return H; }

    std::array<T, W*H> data;
};

template<std::size_t H, std::size_t  W, typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<H, W, T>& m);

template<std::size_t H, std::size_t  W, typename T>
inline bool operator==(const Matrix<H, W, T>& lhm, const Matrix<H, W, T>& rhm) {
    return lhm.data == rhm.data;
}

template<std::size_t H, std::size_t  W, typename T>
inline bool operator!=(const Matrix<H, W, T>& lhm, const Matrix<H, W, T>& rhm){
    return !operator==(lhm,rhm);
}

// Replaced with generic alternatives below.

//template<std::size_t H, std::size_t  W, typename T>
//inline Matrix<W, H, uint32_t> operator+(Matrix<H, W, T> lhm, const Matrix<H, W, T>& rhm) {
//    lhm += rhm;
//    return temp;
//}
//
//template<std::size_t H, std::size_t  W, typename T>
//inline Matrix<H, W, T> operator-(Matrix<H, W, T> lhm, const Matrix<H, W, T>& rhm) {
//    lhm -= rhm;
//    return lhm;
//}

template<std::size_t H, std::size_t W, typename T, typename T2>
inline auto operator+(Matrix<H, W, T>& lhm, const Matrix<H, W, T2>& rhm)
    -> Matrix<H, W, decltype(lhm.data[0] + rhm.data[0])>
{
    Matrix<H, W, decltype(lhm.data[0] + rhm.data[0])> temp;
    
    for (size_t i = 0; i < W*H; i++) {
            temp.data[i] = lhm.data[i] + rhm.data[i];
        }
    return temp;
}

template<std::size_t H, std::size_t W, typename T, typename T2>
inline auto operator-(Matrix<H, W, T>& lhm, const Matrix<H, W, T2>& rhm)
    -> Matrix<H, W, decltype(lhm.data[0] - rhm.data[0])>
{
    Matrix<H, W, decltype(lhm.data[0] - rhm.data[0])> temp;
    
    for (size_t i = 0; i < W*H; i++) {
            temp.data[i] = lhm.data[i] - rhm.data[i];
        }
    return temp;
}


//template<std::size_t H, std::size_t  W, typename T>
//inline bool operator< (const Matrix<H, W, T>& lhm, const Matrix<H, W, T>& rhm){return lhm.list < rhm.list;}
//
//template<std::size_t H, std::size_t  W, typename T>
//inline bool operator> (const Matrix<H, W, T>& lhm, const Matrix<H, W, T>& rhm){return  operator< (rhm,lhm);}
//
//template<std::size_t H, std::size_t  W, typename T>
//inline bool operator<=(const Matrix<H, W, T>& lhm, const Matrix<H, W, T>& rhm){return !operator> (lhm,rhm);}
//
//template<std::size_t H, std::size_t  W, typename T>
//inline bool operator>=(const Matrix<H, W, T>& lhm, const Matrix<H, W, T>& rhm){return !operator< (lhm,rhm);}
   

#include "Matrix.cpp" // Because its a template class.


#endif /* MATRIX_H */

