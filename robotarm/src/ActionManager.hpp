/** 
 * @file   ActionManager.hpp
 * @author Maurice Rozema
 * @author Mustafa Sabur
 */

#ifndef ACTIONMANAGER_HPP
#define ACTIONMANAGER_HPP

#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>

#include "robotarm/MoveAction.h"
#include "robotarm/Insta.h"
#include "robotarm/Joints.h"

/**
 * @brief The ActionManager puts the movements in the queue. Contains an instance of ActionClient.
 */
class ActionManager {
    typedef actionlib::SimpleActionClient<robotarm::MoveAction> ActionClient;
    typedef robotarm::MoveActionFeedback::_feedback_type Feedback;
    typedef robotarm::MoveActionResult::_result_type Result;
    typedef robotarm::MoveActionGoal::_goal_type Goal;
public:

    ActionManager(std::string serverName);

    /**
     * Processes all the movements in the queue.
     */
    void spin();

    /**
     * Wait for server to start.
     */
    void waitForServer();

private:

    /**
     * Sends goal to the action client.
     * @param move Move message to send.
     */
    void onNewMovement(const robotarm::Joints::ConstPtr& move);

    /**
     * Cancels or clears the queue.
     * @param msg Instant message to send.
     */
    void onInstaMsgs(const robotarm::Insta::ConstPtr& msg);

    /**
     * Displays the start and stop time of the movement that was initiated, after the robot is in the wanted position.
     * 
     * @param state Current state. Can contain the following values: PENDING, ACTIVE, RECALLED, REJECTED, PREEMPTED, ABORTED, SUCCEEDED, LOST.
     * @param result Contains the result of the movement.
     */
    void actionDoneCallback(const actionlib::SimpleClientGoalState& state, const Result::ConstPtr& result);

    /**
     * Called when a goal is activated.
     */
    void actionActiveCallback();

    /**
     * Displays the progress in time of the current movement. 
     * @param feedback Contains the feedback that is received from the server.
     */
    void actionFeedbackCallback(const Feedback::ConstPtr& feedback);

    ros::NodeHandle moveNh, instaNh; ///< 
    ros::Subscriber moveSub, instaSub; ///< 
    ros::CallbackQueue queue; ///< 
    ActionClient ac; ///< 
};

#endif /* ACTIONMANAGER_HPP */

