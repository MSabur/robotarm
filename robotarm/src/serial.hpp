/*
 * Serial.hpp
 *
 *  Created on: 18 feb. 2017
 *      Author: Maurice
 */

#ifndef Serial_HPP_
#define Serial_HPP_

#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/bind.hpp>
#include <boost/system/error_code.hpp>
#include <boost/system/system_error.hpp>
#include <boost/thread.hpp>
#include <cstdint>
#include <iostream>
#include <string>

#define READ_BUFFER_SIZE 256
typedef boost::shared_ptr<boost::asio::serial_port> serial_port_ptr;

class Serial
{
  public:
    /**
     * Gets the a reference to a Serial instance.
     **/
    static Serial& getInstance();

    /**
     * Starts the serial communication.
     * @param portName	Portname on the pc the controller is connected to.
     * @param baudrate	Baudrate to run the serial communication on.
     * @return 			Returns true if the serial communication has
     * succesfully been started.
     **/
    bool start(const std::string& portName, uint32_t baudrate);

    /**
     * Stop and performs cleanup of the serial communication.
     **/
    void stop();

    /**
     * Writes a string to the serial port.
     * @param message	String to write to the serial port.
     * @return			Returns the amount of bytes written.
     **/
    int write(const std::string& message);

    virtual ~Serial();

  private:
    Serial();

    /**
     * Writes a message to the serial port.
     * @param buf	A char* that represents the message.
     * @param size	The length of the message.
     **/
    int write_some(const char* buf, const int& size);

    static Serial instance;

    boost::asio::io_service serial_service;
    serial_port_ptr serial_port;
    boost::mutex serial_mutex;
};

#endif /* Serial_HPP_ */
