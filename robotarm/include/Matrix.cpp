
#include <array>
#include <complex>

template<std::size_t H, std::size_t W, typename T>
Matrix<H, W, T>::Matrix(T value) {
    for (size_t i = 0; i < H * W; ++i) {
        data[i] = value;
    }

}

template<std::size_t H, std::size_t W, typename T>
Matrix<H, W, T>::Matrix(std::initializer_list<T>& il) {
    static_assert(il.size() == W * H, "Invalid number of items.");
    std::copy(il.begin(), il.end(), data.begin());

}

//template<std::size_t H, std::size_t W, typename T>
//Matrix<H, W, T>::Matrix(std::initializer_list<std::initializer_list<T>>&il) {
//    static_assert(il.size() == W * H, "Invalid number of items.");
//
//    auto listPos = data.begin();
//    for (size_t i = 0; i < il.size(); ++i) {
//        std::copy(il[i].begin(), il[i].end(), listPos);
//        listPos += il[i].size();
//    }
//}

template<std::size_t H, std::size_t W, typename T>
Matrix<H, W, T>::Matrix(const std::array<T, W*H>& array)
: data(array) {
}

template<std::size_t H, std::size_t W, typename T>
Matrix<H, W, T>& Matrix<H, W, T>::operator=(const Matrix<H, W, T>& mat) {
    //    std::swap(mat);
    //    return *this;
    if (this != &mat) {
        data = mat.data;
    }
    return *this;
}

template<std::size_t H, std::size_t W, typename T>
Matrix<H, W, T>& Matrix<H, W, T>::operator*=(T scalar) {
    for (auto& l : data) {
        l *= scalar;
    }
    return *this;
}

template<std::size_t H, std::size_t W, typename T>
Matrix<H, W, T>& Matrix<H, W, T>::operator/=(T scalar) {
    for (auto& l : data) {
        l /= scalar;
    }
    return *this;
}

template<std::size_t H, std::size_t W, typename T>
template<std::size_t W2>
Matrix<H, W2, T> Matrix<H, W, T>::operator*(const Matrix<W, W2, T>& mat) {
    Matrix<H, W2, T> temp;
    size_t listPos = 0;
    for (size_t h = 0; h < H; h++) {
        for (size_t i = 0; i < W2; i++) {
            T newValue = 0;
            size_t k = i;
            for (typename std::array<T, W*H>::size_type j = 0; j < W; j++) {
                newValue += data[(W * h) + j] * mat.data[k];
                k += (W2);
            }
            temp.data[listPos] = newValue;
            listPos++;
        }
    }
    return temp;
}

template<std::size_t H, std::size_t W, typename T>
Matrix<W, H, T> Matrix<H, W, T>::transpose() const {
    Matrix<W, H, T> temp;
    size_t newIt = 0;
    for (size_t i = 0; i < W; i++) {
        for (size_t j = 0; j < H; j++) {
            temp.data[newIt] = data[(j * W) + i];
            newIt++;
        }
    }
    return temp;
}

template<std::size_t H, std::size_t W, typename T>
Matrix<H, W, T> Matrix<H, W, T>::identity() const {
    Matrix<H, W, T> temp(0);
    for (size_t i = 0; i < data.size(); i += (W + 1)) {
        temp.data[i] = 1;
    }
    return temp;
}

template<std::size_t H, std::size_t W, typename T>
void Matrix<H, W, T>::swapRows(std::size_t a, std::size_t b) {
    assert(a < H && b < H);
    for (size_t i = 0; i < W; ++i) {
        std::swap(data[a * W + i], data[b * W + i]);
    }

}

template<std::size_t H, std::size_t W, typename T>
Matrix<H, W, T> Matrix<H, W, T>::inverse() const {
    static_assert(W == H, "Non-square matrix do not have an inverse.");

    Matrix<H, W, T > left(*this);
    Matrix<H, W, T > right(identity());

    for (std::size_t h = 0; h < H; ++h) {
        T maxValue = 0;
        std::size_t maxValueRow = 0;

        for (size_t row = h; row < H; ++row) {
            T value = std::abs(left.data[W * row + h]);
            if (maxValue < value) {
                maxValue = value;
                maxValueRow = row;
            }
        }

        if (h != maxValueRow) {
            left.swapRows(h, maxValueRow);
            right.swapRows(h, maxValueRow);
        }

        for (size_t row = 0; row < H; ++row) {
            double mtplr = left.data[W * row + h];
            if (row == h) mtplr -= 1; // Minus 1 because we need to create a 1 not a 0.
            mtplr /= left.data[W * h + h];

            for (std::size_t col = 0; col < W; ++col) {
                left.data[W * row + col] -= left.data[W * h + col] * mtplr;
                right.data[W * row + col] -= right.data[W * h + col] * mtplr;
            }
            //            std::cout << left << "\n" << std::endl;
        }
    }
    //    std::cout << left << "\n" << std::endl;
    return right;
}

template<std::size_t H, std::size_t W, typename T>
bool Matrix<H, W, T>::approxEqual(const Matrix<H, W, T>& mat, const T maxDiff) const {
    for (size_t i = 0; i < data.size(); ++i) {
        if (std::abs(data[i] - mat.data[i]) > maxDiff) return false;
    }

    return true;
}

// Bug: OSTREAM_INDENTED mode got trouble with negative values;

template<std::size_t H, std::size_t W, typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<H, W, T>& mat) {
#ifdef OSTREAM_INDENTED
    auto it = std::max_element(std::begin(mat.data), std::end(mat.data));
    auto maxValue = *it;
    int32_t maxLength = 1;
    while (maxValue /= 10) {
        maxLength *= 10;
    }

#endif
    for (std::size_t i = 0; i < W * H; ++i) {
        if ((i) % W == 0 && i != 0) {
            os << std::endl;
        }
#ifdef OSTREAM_INDENTED
        uint32_t currentLength = maxLength;
        while (mat.data[i] < currentLength) {
            os << ' ';
            currentLength /= 10;
        }
#endif
        os << mat.data[i] << ", ";
    }
    return os;
}
