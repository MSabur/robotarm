/*
 * ObjectDetector.hpp
 *
 *  Created on: Feb 24, 2017
 *      Author: maurice
 * 		Email: 	m.rozema2@student.han.nl
 */

#ifndef OBJECTDETECTOR_HPP_
#define OBJECTDETECTOR_HPP_

#include <boost/lexical_cast.hpp>
#include <chrono>
#include <cmath>
#include <ctime>
#include <thread>
#include <vector>

#include "Enums.hpp"
#include "SerialParser.hpp"
#include "Video.hpp"
#include "Sender.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "robotarm/PickAndPlace.h"

class ObjectDetector
{
  public:
    /**
     * Constructor of the Object Detector class.
     * This class will detect and draw all the shapes.
     **/
    explicit ObjectDetector();

    /**
     * Almost the same Constructor as above, but this constructor has to be
     * called when in batch mode.
     *
     * @param pathToBashFile	Path to the batch file
     **/
    explicit ObjectDetector(const std::string& pathToBashFile);

    /**
     * Destructor
     **/
    virtual ~ObjectDetector();

  private:
    /**
     * State machine
     * This statemachine is the main of the program.
     * It will check if there is user input available
     * It will set the treshold for the images
     * It will detect the shapes
     * It will draw the shapes
     * It will print the shapes to the console
     * @see waitForUserInput()
     * @see BashInput()
     **/
    void programState();

    /**
     * This will detect colors in a given matrix.
     * @param e_object_color	Enum of the color
     * @param color_mat			Reference to the matrix, which has the
     *color.
     **/
    void detectColors(object_colors e_object_color, cv::Mat& color_mat);

    /**
     * This will detect shapes in a given matrix.
     * @param e_object_shape	Enum of the shape
     * @param color_mat			Reference to the matrix, which has the
     *shape.
     **/
    void detectShapes(object_shapes e_object_shape, const cv::Mat& color_mat);

    /**
     * This will remove shadow in the image.
     * It is used for pre-processing, so that color and shape can be detected.
     * @param aMat			Reference to the matrix, which has the
     *shadow.
     **/
    void removeShadowEffects(cv::Mat& aMat, object_colors aColor);

    /**
     * This will set a treshhold for a grayscale image.
     * It will blur,erode and put an adaptive treshold the grayscale image.
     * @param aMat			Reference to the matrix, which has to be
     *tresholded.
     **/
    void basicGrayTreshold(cv::Mat& aMat);

    /**
     * This will set a treshhold for a colored image.
     * This will remove the shadows of the image and set all the colors to the
     *same blue.
     * This way, colors can be detected better.
     * @param aMat			Reference to the matrix, which has to be
     *tresholded.
     * @see removeShadowEffects()
     **/
    void basicColorTreshold(cv::Mat& aMat, object_colors aColor);

    /**
     * Checks if a rectangle is a square.
     * @param aRect		rectangle
     * @return True if the rectangle is a square.
     **/
    bool isSquare(const cv::vector<cv::Point>& contour);

    /**
     * Checks if a rectangle is a rectangle and not a square of a triangle.
     * @param contour		The contour to see it.
     * @return True if the rectangle is a rectangle.
     **/
    bool isRectangle(const cv::vector<cv::Point>& contour);

    /**
     * Checks if a contour is a half circle.
     * @param contour		The contour to see it.
     * @return True if the contour is a half circle.
     **/
    bool isHalfCircle(const cv::vector<cv::Point>& contour);

    /**
     * Draw the detected shapes on the given image.
     * @param aMat		Matrix to draw on.
     * @param contours Contours to draw.
     * @param circles Circles to draw.
     * @see detectShapes()
     **/
    void drawShapes(const cv::Mat& aMat,
                    const std::vector<std::vector<cv::Point>>& contours,
                    const std::vector<cv::Vec3f>& circles);

    /**
     * Print the specifications from a contour to the console.
     */
    void contourToText();

    /**
     * Save the position of the result to the resultCircle vec
     */
    bool savePositionResult();

    /**
     * Get difference of the X,Y and Z in pixels.
     */
    robotarm::PickAndPlace calculateDifference(std::vector<cv::Point>, cv::Vec3f);

    /**
     * Get difference of the X, Y and Z of a circle in pixels.
     */ 
    robotarm::PickAndPlace calculateDifference(cv::Vec3f, cv::Vec3f);

    std::vector<std::vector<cv::Point>> right_contours;
    cv::Vec3f resultCircle;
    cv::Vec3f zeroPoint;
    std::vector<cv::Vec3f> circles;
    std::vector<std::pair<object_shapes, object_colors>> object_specifications;
    std::pair<object_shapes, object_colors> current_object;
    program_states e_program_state;
    SerialParser serial_parser;
    Sender sender;
    Video video_thread;
    cv::Mat origin_image, color_image, result_window;
    std::string bash_file;
    clock_t beginClock, endClock;
    robotarm::PickAndPlace message;
    const int DIAMETER_OF_CIRCLE = 10; // IN CM
    const int OFFSET_TO_ROBOTARM = 50;
    const int OFFSET_TO_GROUND = -35;
};

#endif /* OBJECTDETECTOR_HPP_ */
