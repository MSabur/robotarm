/** 
 * @file   LowInterface.hpp
 * @author Maurice Rozema
 * @author Mustafa Sabur
 */


#ifndef LOWINTERFACE_HPP
#define LOWINTERFACE_HPP
#include <ros/ros.h>
#include <sstream>
#include <cstdlib>
#include <std_msgs/String.h>
#include <actionlib/server/simple_action_server.h>
#include <robotarm/MoveAction.h>
#include "robotarm/Insta.h"
#include "robotarm/Joints.h"
#include "serial.hpp"

#define DEFAULT_PORT "/dev/ttyUSB0"
#define BAUDRATE 9600
#define REFRESH_RATE 30 // Spin rate in Hz
#define FEEDBACK_RATE 10 // Feedback rate in Hz, send to ActionServer

/**
 * @brief The SSC32 writes serial commands to the actual hardware.
 */
class SSC32 {
public:
    /**
     * Creates a command that is sent to the serial port.
     * @param move Based on the given move a command is composed.
     * @return Returns the composed command string.
     */
    std::string createCMD(robotarm::Joints& move);
    
    robotarm::Joints jointsFromCMD(std::string cmd);
    /**
     * Creates a string that contains a stop command.
     * @return A string that contains a serial stop command.
     */
    std::string createStopCMD();
private:
    /**
     * Maps a variable to another scaling.
     * @param x The variable that is to be scaled.
     * @param in_min The original scale minimum.
     * @param in_max The original scale maximum.
     * @param out_min The wanted scale minimum.
     * @param out_max The wanted scale maximum.
     * @return 
     */
    int16_t remap(int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max);

    const std::string MAX_SPEED = "S1000"; ///< Maximum servospeed in milliseconds per second
};

/**
 * @brief The LowInterface writes serial commands to SSC32. Converts a HighInterface message to a serial message. Contains an instance of ActionServer.
 */
class LowInterface {
    typedef actionlib::SimpleActionServer<robotarm::MoveAction> ActionServer;
    typedef robotarm::MoveActionFeedback::_feedback_type Feedback;
    typedef robotarm::MoveActionResult::_result_type Result;
    typedef robotarm::MoveActionGoal::_goal_type Goal;

public:
    /**
     * Constructor
     * @param name The name of the LowInterface.
     * @param port The serial port to which the robot is connected. Is by default the DEFAULT_PORT define.
     * @param baudrate The baudrate of the connected robot. Is by default the BAUDRATE define.
     */
    LowInterface(std::string name, const std::string& port, uint32_t baudrate = BAUDRATE);

    virtual ~LowInterface();

    /**
     * Checks if the goal of the robot is reached and gives feedback during the movement of the robot.
     */
    void spin();

private:

    /**
     * Goal callback function writes the message to the serial port.
     */
    void goalCB();

    /**
     * Preempt callback function is called when an emergency stop is initiated.
     */
    void preemptCB();

    ros::NodeHandle nh; ///< 
    ActionServer server; ///< 
    Feedback feedback; ///< 
    Result result; ///< 
    Goal goal;
    ros::Publisher pub; ///< 
    std_msgs::String msg; ///<  
    ros::Time expectedEnd; ///<  
    SSC32 ssc32; ///< 
};

#endif /* LOWINTERFACE_HPP */

