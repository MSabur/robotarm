## Afmeting van de cirkel

De diameter van de cirkel waar het blokje op gezet moet worden staat standaard op 10CM. Als je de diameter wilt aanpassen moet je dit doen in de ObjectDetector.hpp. Onderaan, bij de variabele staat: "const int DIAMETER_OF_CIRCLE = 10; // IN CM". Pas dit aan naar de gewenste diameter. Naderhand opnieuw compileren en klaar.

## Installatie:

- Clone repo in 'src' map van je catkin workspace
- Catkin_make
- Klaar
- Om het programma te runnen is er een launchfile gemaakt. "roslaunch opencv_bp2 opencv_bp2.launch"

- Errors
    - Op het volgende systeem is het gecompileerd en werkend bevonden.
        - OpenCV 2
        - Boost 1.58
        - ROS Kinetic (Update 27-09-17)
        - CMake 3.5.1
        - GCC 6.3.0