/*
* Sender.cpp
*
*  Created on: Sep 27, 2017
*      Author: maurice
*/

#include "Sender.hpp"

Sender::Sender() :
		chatter_pub(nh.advertise < robotarm::PickAndPlace > ("DetectedShape", 1000)) {
}

Sender::~Sender() {
	// TODO Auto-generated destructor stub
}

void Sender::sendMessage(robotarm::PickAndPlace& message) {
	if(ros::ok()) {
		chatter_pub.publish(message);
		ros::spinOnce();
	} else {
		std::cout << "Ros not ok" << std::endl;
	}
}

