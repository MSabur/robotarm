#include "Kinematics.hpp"

Kinematics::Kinematics() {
}

Kinematics::~Kinematics() {
}

bool Kinematics::check(robotarm::Joints& joints) {
    cout << "checking..." << endl;
    if (joints.base < joints.BASE_MIN || joints.base > joints.BASE_MAX) {
        return false;
    }
    if (joints.shoulder < joints.SHOULDER_MIN || joints.shoulder > joints.SHOULDER_MAX) {
        return false;
    }
    if (joints.elbow < joints.ELBOW_MIN || joints.elbow > joints.ELBOW_MAX) {
        return false;
    }
    if (joints.wrist < joints.WRIST_MIN || joints.wrist > joints.WRIST_MAX) {
        return false;
    }
    if (joints.gripper < joints.GRIPPER_MIN || joints.gripper > joints.GRIPPER_MAX) {
        return false;
    }
    if (joints.wristRotation < joints.WRISTROTATION_MIN || joints.wristRotation > joints.WRISTROTATION_MAX) {
        return false;
    }
    return true;
}

Matrix<2, 1, double> Kinematics::forward(double a1, double a2, double a3, double x, double y, double l1, double l2, double l3) {
    Matrix<2, 1, double> m;
    m.at(0, 0) = (x + l1 * sin(a1) + l2 * sin(a1 + a2) + l3 * sin(a1 + a2 + a3));
    m.at(1, 0) = (y + l1 * cos(a1) + l2 * cos(a1 + a2) + l3 * cos(a1 + a2 + a3));
    return m;
}

Matrix<2, 3, double> Kinematics::getJacobian(double a0, double a1, double a2) {
    Matrix<2, 3, double> j;
    j.at(0, 2) = WRIST_GRIPPER_MM * cos(a0 + a1 + a2);
    j.at(0, 1) = j.at(0, 2) + ELBOW_WRIST_MM * cos(a0 + a1);
    j.at(0, 0) = j.at(0, 1) + SHOULDER_ELBOW_MM * cos(a0);

    j.at(1, 2) = -WRIST_GRIPPER_MM * sin(a0 + a1 + a2);
    j.at(1, 1) = j.at(1, 2) - ELBOW_WRIST_MM * sin(a0 + a1);
    j.at(1, 0) = j.at(1, 1) - SHOULDER_ELBOW_MM * sin(a0);
    return j;
}

bool Kinematics::inverse(const Point& p, robotarm::Joints& joints) {

    const double BASE_GOAL_HR_MM = sqrt(pow(p.x, 2) + pow(p.y, 2));
    const double BASE_GOAL_MM = sqrt(pow((BASE_GOAL_HR_MM), 2) + pow(p.z, 2));

    if ((BASE_GOAL_MM > SHOULDER_ELBOW_MM + ELBOW_WRIST_MM) ||
            (BASE_GOAL_MM <= abs(SHOULDER_ELBOW_MM - ELBOW_WRIST_MM))) {
        cout << "It's unreachable." << endl;
        return false;
    }

    Matrix<2, 1, double> goal;
    goal.at(0, 0) = BASE_GOAL_HR_MM;
    goal.at(1, 0) = p.z;

    cout << goal << endl;

    Matrix<2, 1, double> estimation;
    Matrix<3, 1, double> angles;
    angles.at(0, 0) = DEG_TO_RAD(joints.shoulder);
    angles.at(1, 0) = DEG_TO_RAD(joints.gripper);
    angles.at(2, 0) = DEG_TO_RAD(joints.wrist);
    cout << angles << endl;    
    estimation = forward(angles.at(0, 0), angles.at(1, 0), angles.at(2, 0));
    cout << estimation << endl;

    joints.base = RAD_TO_DEG(asin(p.x / BASE_GOAL_HR_MM));
    if (goal.at(0, 0) < 0) joints.base *= -1;

    uint16_t loops = 0;
    while (!estimation.approxEqual(goal, 1) || !check(joints)) {
        if (loops == 250) return false;
        ++loops;

        Matrix<2, 3, double> jacobian = getJacobian(angles.at(0, 0), angles.at(1, 0), angles.at(2, 0));
        Matrix<2, 1, double> deltaEstimation = (goal - estimation);
        deltaEstimation *= 0.4;
        Matrix<3, 2, double> transJacobian = jacobian.transpose();
        Matrix<3, 2, double> inverseJacobian = transJacobian * (jacobian * transJacobian).inverse();

        Matrix<3, 1, double> deltaAngles = inverseJacobian * deltaEstimation;
        angles = angles + deltaAngles;
        estimation = forward(angles.at(0, 0), angles.at(1, 0), angles.at(2, 0));
        //        cout << estimation << endl;
        
        joints.shoulder = RAD_TO_DEG(angles.at(0, 0));
        joints.elbow = RAD_TO_DEG(angles.at(1, 0));
        joints.wrist = RAD_TO_DEG(angles.at(2, 0));

    }
    return true;
}

bool Kinematics::inverseWithCosineRule(const Point& goal, robotarm::Joints& joints, double grabAngle) {
    if (fabs(grabAngle) > pi / 2) {
        cout << "Unsupported grab angle: " << RAD_TO_DEG(grabAngle) << endl;
        return false;
    }

    const double BASE_GOAL_HR_MM = sqrt(pow(goal.x, 2) + pow(goal.y, 2));
    const double WRIST_GRIPPER_HOR_MM = cos(grabAngle) * WRIST_GRIPPER_MM;
    const double WRIST_HEIGHT = -(sin(grabAngle) * WRIST_GRIPPER_MM) + goal.z;
    const double SHOULDER_WRIST_MM = sqrt(pow((BASE_GOAL_HR_MM - WRIST_GRIPPER_HOR_MM), 2) + pow(WRIST_HEIGHT, 2));

    if ((SHOULDER_WRIST_MM > SHOULDER_ELBOW_MM + ELBOW_WRIST_MM) ||
            (SHOULDER_WRIST_MM <= abs(SHOULDER_ELBOW_MM - ELBOW_WRIST_MM))) {
        cout << "It's unreachable." << endl;
        return false;
    }

    double a, b;
    if (SHOULDER_WRIST_MM == SHOULDER_ELBOW_MM + ELBOW_WRIST_MM) {
        a = 0;
        b = pi;
    }
    else {
        a = acos((pow(SHOULDER_ELBOW_MM, 2) + pow(SHOULDER_WRIST_MM, 2) - pow(ELBOW_WRIST_MM, 2)) /
                (2 * SHOULDER_ELBOW_MM * SHOULDER_WRIST_MM));

        b = acos((pow(ELBOW_WRIST_MM, 2) + pow(SHOULDER_ELBOW_MM, 2) - pow(SHOULDER_WRIST_MM, 2)) /
                (2 * ELBOW_WRIST_MM * SHOULDER_ELBOW_MM));
    }

    const double f = asin(WRIST_HEIGHT / SHOULDER_WRIST_MM);

    joints.base = RAD_TO_DEG(asin(fabs(goal.x) / BASE_GOAL_HR_MM));
    if (goal.x < 0) joints.base *= -1;
    joints.shoulder = RAD_TO_DEG(pi * 0.5 - a - f);
    joints.elbow = RAD_TO_DEG(pi - b);
    joints.wrist = RAD_TO_DEG(pi - b - a - f + grabAngle);

    cout << joints << endl;

    cout << "Checking joint bounds.." << endl;
    return check(joints);
    
}
