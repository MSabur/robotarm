#include "serial.hpp"

Serial& Serial::getInstance()
{
    static Serial instance;
    return instance;
}

Serial::Serial()
{
    // TODO Auto-generated constructor stub
}

bool Serial::start(const std::string& portName, uint32_t baudrate)
{
    boost::system::error_code ec;

    if (serial_port) {
        std::cout << "error: port is already opened..." << std::endl;
        return false;
    }

    serial_port = serial_port_ptr(new boost::asio::serial_port(serial_service));
    serial_port->open(portName, ec);
    if (ec) {
        std::cout << "error: can't open serial port." << portName << " Error = " << ec.message().c_str() << std::endl;
        return false;
    }

    // option settings...
    serial_port->set_option(boost::asio::serial_port_base::baud_rate(baudrate));
    serial_port->set_option(boost::asio::serial_port_base::character_size(8));
    serial_port->set_option(boost::asio::serial_port_base::stop_bits(
        boost::asio::serial_port_base::stop_bits::one));
    serial_port->set_option(boost::asio::serial_port_base::parity(
        boost::asio::serial_port_base::parity::none));
    serial_port->set_option(boost::asio::serial_port_base::flow_control(
        boost::asio::serial_port_base::flow_control::none));

    boost::thread t(boost::bind(static_cast<size_t (boost::asio::io_service::*)()>(
                        &boost::asio::io_service::run),
                    &serial_service));

    return true;
}

void Serial::stop()
{
    boost::mutex::scoped_lock look(serial_mutex);

    if (serial_port) {
        serial_port->cancel();
        serial_port->close();
        serial_port.reset();
    }

    serial_service.stop();
    serial_service.reset();
}

int Serial::write(const std::string& message)
{
    return write_some(message.c_str(), message.size());
}

int Serial::write_some(const char* buf, const int& size)
{
    boost::system::error_code ec;

    if (!serial_port)
        return -1;
    if (size == 0)
        return 0;

    return serial_port->write_some(boost::asio::buffer(buf, size), ec);
}

Serial::~Serial()
{
    stop();
}
