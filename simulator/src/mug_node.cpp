#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/InteractiveMarker.h>
#include <interactive_markers/interactive_marker_server.h>

#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <thread>

#define MUG_HEIGHT .1    // 10 cm
#define MUG_RADIUS 0.05  // 5cm

visualization_msgs::Marker g_marker;

using namespace std;

bool g_grabbed = false;

void checkIfGrabbed()
{
  ros::Rate r(10);
  tf::TransformListener listener;
  tf::StampedTransform tran;

  while (ros::ok())
  {
    try
    {
      listener.waitForTransform("gripper_left", "mug", ros::Time(0), ros::Duration(1.0));
      listener.lookupTransform("gripper_left", "mug", ros::Time(0), tran);

      tf::Vector3 check = tran.getOrigin().absolute();

      if (check.getX() < MUG_HEIGHT / 2 && check.getY() < 0.026 && check.getZ() < 0.026)
      {
        cout << "Grabbed it!" << endl;
        g_grabbed = true;
      }
      else
      {
        cout << "No Grab!" << endl;
        g_grabbed = false;
      }
    }
    catch (tf::TransformException& ex)
    {
      ROS_ERROR("%s", ex.what());
      ros::Duration(1.0).sleep();
    }
    r.sleep();
  }
}

void processFeedback(
  const visualization_msgs::InteractiveMarkerFeedbackConstPtr feedback )
{
  g_marker.pose = feedback->pose;
  std::cout << feedback.get()->marker_name << "," << feedback.get()->pose.position << std::endl;
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "mug_node");

  interactive_markers::InteractiveMarkerServer server("mug_marker");
  // create an interactive marker for our server
  visualization_msgs::InteractiveMarker int_marker;
  int_marker.header.frame_id = "base_link";
  int_marker.header.stamp = ros::Time::now();
  int_marker.name = "mug";
  int_marker.description = "mug_controls";

  ros::NodeHandle n;
  ros::Rate r(30);
  tf::TransformBroadcaster br;

  g_marker.header.frame_id = "base_link";
  g_marker.header.stamp = ros::Time::now();
  g_marker.ns = "mug";
  g_marker.id = 0;

  g_marker.type = visualization_msgs::Marker::CYLINDER;

  g_marker.scale.x = MUG_RADIUS;
  g_marker.scale.y = MUG_RADIUS;
  g_marker.scale.z = MUG_HEIGHT;

  // RED
  g_marker.color.r = 1;
  g_marker.color.g = 0;
  g_marker.color.b = 0;
  g_marker.color.a = 1;

  // Zero rotation in all the axes
  g_marker.pose.orientation.x = 0;
  g_marker.pose.orientation.y = 0;
  g_marker.pose.orientation.z = 0;
  g_marker.pose.orientation.w = 1;

  //
  g_marker.pose.position.x = 0.0;
  g_marker.pose.position.y = 0.4;
  g_marker.pose.position.z = 2;

  visualization_msgs::InteractiveMarkerControl box_control;
  box_control.always_visible = true;
  box_control.markers.push_back( g_marker );
 
  int_marker.controls.push_back( box_control );
 
  visualization_msgs::InteractiveMarkerControl rotate_control;
  rotate_control.interaction_mode =
      visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
  rotate_control.orientation.w = 1;
  rotate_control.orientation.x = 1;
  rotate_control.orientation.y = 0;
  rotate_control.orientation.z = 0;
  rotate_control.name = "move_x";
  int_marker.controls.push_back(rotate_control);
  rotate_control.orientation.w = 1;
  rotate_control.orientation.x = 0;
  rotate_control.orientation.y = 0;
  rotate_control.orientation.z = 1;
  rotate_control.name = "move_y";
  int_marker.controls.push_back(rotate_control);
  rotate_control.orientation.w = 1;
  rotate_control.orientation.x = 0;
  rotate_control.orientation.y = 1;
  rotate_control.orientation.z = 0;
  rotate_control.name = "move_z";
  int_marker.controls.push_back(rotate_control);
 
 
  server.insert(int_marker, &processFeedback);
  server.applyChanges();

  std::thread grab_thread(checkIfGrabbed);

  bool cup_spawn = false;
  std::thread delay_thread{[&cup_spawn](){
      std::this_thread::sleep_for(std::chrono::milliseconds(2000));
      cup_spawn = true;}};

  while (ros::ok())
  {
    g_marker.header.stamp = ros::Time::now();

    tf::Transform transform;
    tf::Pose p;
    tf::poseMsgToTF(g_marker.pose, p);
    transform.setOrigin(p.getOrigin());
    transform.setRotation(p.getRotation());
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "mug"));

    if (g_grabbed)
    {
      tf::TransformListener listener;
      tf::StampedTransform tran;

      try
      {
        listener.waitForTransform("base_link", "grip_point", ros::Time(0), ros::Duration(1.0));
        listener.lookupTransform("base_link", "grip_point", ros::Time(0), tran);

        g_marker.pose.position.x = tran.getOrigin().getX();
        g_marker.pose.position.y = tran.getOrigin().getY();
        g_marker.pose.position.z = tran.getOrigin().getZ();
      }
      catch (tf::TransformException& ex)
      {
        ROS_ERROR("%s", ex.what());
        ros::Duration(1.0).sleep();
      }
    }
    else
    {
      if (g_marker.pose.position.z > MUG_HEIGHT / 2 && cup_spawn)
      {
        g_marker.pose.position.z -= 0.5 / 30;
      }
    }

    server.setPose("mug", g_marker.pose);
    server.applyChanges();
    ros::spinOnce();

    r.sleep();
  }

  grab_thread.join();
  return 0;
}
