/*
 * Video.hpp
 *
 *  Created on: Mar 16, 2017
 *      Author: maurice
 * 		Email: 	m.rozema2@student.han.nl
 */

#ifndef VIDEO_HPP_
#define VIDEO_HPP_

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <mutex>

class Video
{
  public:
    cv::VideoCapture video;

    /**
     * Constructor of the Video class.
     * This will display a video mat.
     **/
    Video()
        : video(1)
    {
        if (!video.isOpened())
            std::cout << "Not opened" << std::endl;
    }

    void video_capture()
    {
        while (true) {
            vid_mutex.lock();
            video >> video_cap;
            cv::line(video_cap, cv::Point(0, video_cap.rows / 2), cv::Point(video_cap.cols, video_cap.rows / 2), cv::Scalar(0,0,255));
            vid_mutex.unlock();
            cv::imshow("Video_Window", video_cap);
            cv::waitKey(50);
        }
    }

    cv::Mat getWebcamFrame()
    {
        cv::Mat camFrame, videoFrame;

        vid_mutex.lock();
        video.read(camFrame);
        videoFrame = camFrame.clone();
        vid_mutex.unlock();
        return videoFrame;
    }

  private:
    std::mutex vid_mutex;
    cv::Mat video_cap;
};

#endif /* VIDEO_HPP_ */
