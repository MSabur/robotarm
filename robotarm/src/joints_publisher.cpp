#include <string>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/JointState.h>

#define PUBLISH_RATE 30
#define NUMBER_OF_JOINTS 6
#define GRIPPER_NUMBER 5
#define GRIPPER_ACCURACY 0.0001  // 0.1 mm
#define GRIPPER_SPEED 0.0250     // 2.5 cm/s

#define JOINT_ACCURACY 0.00872665  // 0.5 deg
#define JOINT_SPEED M_PI_4         // 45 deg/s
using namespace std;

sensor_msgs::JointState g_current_joint_state;
sensor_msgs::JointState g_desired_joint_state;

namespace Servo
{
enum Servos
{
  BASE_MIN = 500,
  BASE_MAX = 2400,
  SHOULDER_MIN = 2500,
  SHOULDER_MAX = 500,
  ELBOW_MIN = 700,
  ELBOW_MAX = 1900,
  WRIST_MIN = 500,
  WRIST_MAX = 2500,
  WRISTROTATION_MIN = 500,
  WRISTROTATION_MAX = 2500,
  GRIPPER_MIN = 500,
  GRIPPER_MAX = 2500
};
}
typedef Servo::Servos Servos;

double remap(double x, double in_min, double in_max, double out_min, double out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void commandCB(const std_msgs::String::ConstPtr& command)
{
  std::string cmd = command->data;
  // Clear all the whitespace if any.
  cmd.erase(
      std::remove_if(cmd.begin(), cmd.end(), [](char ch) { return std::isspace<char>(ch, std::locale::classic()); }),
      cmd.end());

  bool keep_parsing = true;
  string substr;
  size_t pos = 0;
  while (keep_parsing)
  {
    pos = cmd.find_first_of("#T", 1);

    // if not found
    if (pos == string::npos)
    {
      substr = cmd;
      keep_parsing = false;
    }
    else
    {
      substr = (cmd.substr(0, pos));
      cmd.erase(0, pos);
    }

    if (substr[0] == '#')
    {
      int16_t speed = 0;
      int16_t pwm = 0;

      if ((pos = substr.find_first_of('S')) != std::string::npos)
      {
        speed = std::stoi(substr.substr(pos + 1));
        substr.erase(pos);
      }
      if ((pos = substr.find_first_of('P')) != std::string::npos)
      {
        pwm = std::stoi(substr.substr(pos + 1));
        substr.erase(pos);
      }
      int16_t servo_number = std::stoi(substr.substr(1));
      if (pwm != 0)
      {
        ROS_INFO_STREAM("pwm: " << pwm);
        switch (servo_number)
        {
          case 0:
            g_desired_joint_state.position[servo_number] = remap(pwm, Servos::BASE_MIN, Servos::BASE_MAX, -1.57, 1.57);
            break;
          case 1:
            g_desired_joint_state.position[servo_number] =
                remap(pwm, Servos::SHOULDER_MIN, Servos::SHOULDER_MAX, -1.57, 1.57);
            break;
          case 2:
            g_desired_joint_state.position[servo_number] = remap(pwm, Servos::ELBOW_MIN, Servos::ELBOW_MAX, 0, 2.356);
            break;
          case 3:
            g_desired_joint_state.position[servo_number] =
                remap(pwm, Servos::WRIST_MIN, Servos::WRIST_MAX, -1.57, 1.57);
            break;
          case 4:
            g_desired_joint_state.position[servo_number] =
                remap(pwm, Servos::WRISTROTATION_MIN, Servos::WRISTROTATION_MAX, -1.57, 1.57);
            break;
          case 5:
            g_desired_joint_state.position[servo_number] =
                remap(pwm, Servos::GRIPPER_MIN, Servos::GRIPPER_MAX, -0.02, 0.02);
            break;
          default:
            break;
        }
      }
    }
  }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "joints_publisher");
  ros::NodeHandle nh;
  ros::Subscriber sub = nh.subscribe("SSC32Commands", 1, commandCB);
  ros::Publisher jointPub = nh.advertise<sensor_msgs::JointState>("joint_states", 1);
  ros::Rate rate(PUBLISH_RATE);
  double diff[NUMBER_OF_JOINTS];

  g_current_joint_state.name.resize(NUMBER_OF_JOINTS);
  g_current_joint_state.position.resize(NUMBER_OF_JOINTS);
  g_current_joint_state.name[0] = "base_link2turret";
  g_current_joint_state.name[1] = "turret2upperarm";
  g_current_joint_state.name[2] = "upperarm2forearm";
  g_current_joint_state.name[3] = "forearm2wrist";
  g_current_joint_state.name[4] = "wrist2hand";
  g_current_joint_state.name[5] = "gripper_left2hand";

  for (int i = 0; i < NUMBER_OF_JOINTS; i++)
  {
    g_current_joint_state.position[i] = 0;
    diff[i] = 0;
  }
  g_desired_joint_state = g_current_joint_state;

  double step = 0;
  int times = 1;
  while (ros::ok())
  {
    // update joint_state
    g_current_joint_state.header.stamp = ros::Time::now();
    // send the joint state and transform
    jointPub.publish(g_current_joint_state);

    for (int i = 0; i < NUMBER_OF_JOINTS; i++)
    {
      if (i == GRIPPER_NUMBER)
      {
        step = GRIPPER_ACCURACY;
        times = GRIPPER_SPEED / GRIPPER_ACCURACY / PUBLISH_RATE;
      }
      else
      {
        step = JOINT_ACCURACY;
        times = JOINT_SPEED / JOINT_ACCURACY / PUBLISH_RATE;
      }
      if (times < 1)
        times = 1;

      for (int j = 0; j < times; j++)
      {
        diff[i] = g_desired_joint_state.position[i] - g_current_joint_state.position[i];
        if (std::fabs(diff[i]) >= step * 2)
        {
          if (diff[i] < 0)
          {
            g_current_joint_state.position[i] -= step;
          }
          else
          {
            g_current_joint_state.position[i] += step;
          }
        }
      }
    }

    ros::spinOnce();
    rate.sleep();
  }

  return 0;
}