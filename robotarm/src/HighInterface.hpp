/** 
 * @file   HighInterface.hpp
 * @author Maurice Rozema
 * @author Mustafa Sabur
 */

#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include <iostream>
#include <ros/ros.h>
#include <robotarm/Joints.h>
#include "robotarm/Insta.h"

/**
 * @brief The HighInterface is exposed to the user. The user communicates with the HighInterface.
 */
class HighInterface {
public:

    /**
     * Constructor
     */
    HighInterface();

    /**
     * @param withValidation If true, the user will be asked to enter yes or no, after command is given. Otherwise the movement will proceed, without asking. Standard value is true.
     * @return Returns true when robot can be moved succesfully, otherwise returns false.
     */
    bool createMove(bool withValidation = true);


    /**
     * @brief  
     * @return 
     */
    bool createInstaMsg();

    /**
     * @param stream Stream that is readed from the commandline.
     * @return Returns true if the given stream causes that the stream is saved, otherwise returns false.
     */
    bool getUserInput(std::istream& stream = std::cin);

    /**
     * Publishes the move to the lowinterface. The robot will move.
     * @param aMove The movement to publish
     */
    void publishMove(robotarm::Joints& aMove);
    
    /**
     * Publishes the move to the lowinterface. The robot will move.
     */
    void publishMove();

    /**
     * Publishes the move to the lowinterface. The robot will move instantaneously.
     */
    void publishInstaMsg();

    /**
     * @param value Value to set in which stance the robot needs to be set.
     * @return Returns true if the given value is found, otherwise returns false.
     */
    bool setStance(const std::string& value);

    /**
     * Waits for subscribers on the 'MovementsQue' topic.
     */
    void waitForSubs();

    /**
     * @return Returns the private #exit variable.
     */
    bool quit();
private:

    /**
     * Prints the given movement for every angle.
     */
    void printJoints();

    /**
     * Prints all the possible commands and its parameters.
     */
    void printHelp();

    /**
     * Calls for an extra validation. The user can enter yes or no.
     * @return Returns true when user enters yes. Returns false when user enters no.
     */
    bool userValidate();

    /**
     * Sets all the movement variables to unset.
     */
    void setDefaults();

    bool exit = false;                  ///< Returned when quit() function is called.
    robotarm::Joints move;                ///< 
    robotarm::Insta instaMsg;           ///< 
    std::vector<std::string> args;      ///< 

    ros::NodeHandle nh;                 ///< 
    ros::Publisher instaPub;            ///< 
    ros::Publisher movePub;             ///< 

};


#endif /* INTERFACE_HPP */

