#include <iomanip>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "ActionManager.hpp"

ActionManager::ActionManager(std::string serverName) :
ac(serverName) {
    instaSub = instaNh.subscribe("InstaMsgs", 2, &ActionManager::onInstaMsgs, this);
    moveNh.setCallbackQueue(&queue);
    moveSub = moveNh.subscribe("MovementsQue", 20, &ActionManager::onNewMovement, this);
}

void ActionManager::spin() {
    ros::Rate r(30); // 30 hz
    while (ros::ok()) {

        if (!queue.isEmpty()) {
            queue.callOne();
            r.sleep();
            ac.waitForResult();
        }
        ros::spinOnce();
        r.sleep();
    }
}

void ActionManager::waitForServer() {
    ROS_INFO("Waiting for action server to start.");
    ac.waitForServer();
    ROS_INFO("Server is running...");
}

void ActionManager::onNewMovement(const robotarm::Joints::ConstPtr& move) {
    ROS_INFO("\nSending new move to server...");
    Goal g;
    g.move = *move;
    ac.sendGoal(g,
            boost::bind(&ActionManager::actionDoneCallback, this, _1, _2),
            boost::bind(&ActionManager::actionActiveCallback, this),
            boost::bind(&ActionManager::actionFeedbackCallback, this, _1));
}

void ActionManager::onInstaMsgs(const robotarm::Insta::ConstPtr& msg) {
    if (msg->memo == "stop") {
        queue.clear();
        ac.cancelGoal();
        ROS_INFO("\nCanceling goal");
    } else if (msg->memo == "clear") {
        queue.clear();
        ROS_INFO("\nClearing queue");
    }
}

void ActionManager::actionDoneCallback(const actionlib::SimpleClientGoalState& state, const Result::ConstPtr& result) {
    ROS_INFO("\nResult: ");
    time_t start = result->start.sec;
    time_t end = result->end.sec;
    std::cout << "\tStarting time:\t" << std::put_time(localtime(&start), "%d-%m-%Y %H:%M:%S") << std::endl;
    std::cout << "\tEnding time:\t" << std::put_time(localtime(&end), "%d-%m-%Y %H:%M:%S") << std::endl;
}

void ActionManager::actionActiveCallback() {
    ROS_INFO("Progress:");
}

void ActionManager::actionFeedbackCallback(const Feedback::ConstPtr& feedback) {
    std::cout << '\r' << std::flush << '\t' << feedback->progress << " sec";
}