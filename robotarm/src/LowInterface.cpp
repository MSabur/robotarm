#include "LowInterface.hpp"

#define SERIAL
using namespace std;

enum Servos {
    BASE_MIN = 500,
    BASE_MAX = 2400,
    SHOULDER_MIN = 1900,
    SHOULDER_MAX = 900,
    ELBOW_MIN = 700,
    ELBOW_MAX = 1900,
    WRIST_MIN = 500,
    WRIST_MAX = 2500,
    WRISTROTATION_MIN = 500,
    WRISTROTATION_MAX = 2500,
    GRIPPER_MIN = 500,
    GRIPPER_MAX = 2500
};

int16_t SSC32::remap(int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max) {

    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

string SSC32::createStopCMD() {
    return "STOP\r";
}


robotarm::Joints SSC32::jointsFromCMD(std::string cmd) {
    robotarm::Joints joints;

    // Clear all the whitespace if any.
    cmd.erase(std::remove_if(cmd.begin(), cmd.end(),
            [] (char ch) {
                return std::isspace<char>(ch, std::locale::classic());
            }), cmd.end());


    bool keepParsing = true;
    string substr;
    size_t pos = 0;
    while (keepParsing) {
    
        pos = cmd.find_first_of("#T", 1);
        
        // if not found
        if (pos == string::npos) {
            substr = cmd;
            keepParsing = false; 
        } else {
           substr = (cmd.substr(0, pos));
           cmd.erase(0, pos);
        }

        if (substr[0] == '#') {
            int16_t pwm = joints.UNSET;
            int16_t servoNr = joints.UNSET;

            if ((pos = substr.find_first_of('S')) != std::string::npos) {
                substr.erase(pos);
            }
            if ((pos = substr.find_first_of('P')) != std::string::npos) {
                pwm = std::stoi(substr.substr(pos + 1));
                substr.erase(pos);
            }
            servoNr = std::stoi(substr.substr(1));

            switch (servoNr) {
                case 0:
                    joints.base = remap(pwm, Servos::BASE_MIN, Servos::BASE_MAX, robotarm::Joints::BASE_MIN, robotarm::Joints::BASE_MAX);
                    break;
                case 1:
                    joints.shoulder = remap(pwm, Servos::SHOULDER_MIN, Servos::SHOULDER_MAX, robotarm::Joints::SHOULDER_MIN, robotarm::Joints::SHOULDER_MIN);
                    break;
                case 2:
                    joints.elbow = remap(pwm, Servos::ELBOW_MIN, Servos::ELBOW_MAX, robotarm::Joints::ELBOW_MIN, robotarm::Joints::ELBOW_MAX);
                    break;
                case 3:
                    joints.wrist = remap(pwm, Servos::WRIST_MIN, Servos::WRIST_MAX, robotarm::Joints::WRIST_MIN, robotarm::Joints::WRIST_MAX);
                    break;
                case 4:
                    joints.wristRotation = remap(pwm, Servos::WRISTROTATION_MIN, Servos::WRISTROTATION_MAX, robotarm::Joints::WRISTROTATION_MIN, robotarm::Joints::WRISTROTATION_MAX);
                    break;
                case 5:
                    joints.gripper = remap(pwm, Servos::GRIPPER_MIN, Servos::GRIPPER_MAX, robotarm::Joints::GRIPPER_MIN, robotarm::Joints::GRIPPER_MAX);
                    break;
                default:
                    break;
            }

        } else joints.time = std::stoi(substr.substr(1));
    }

    cout << joints << endl;
    return joints;
}

string SSC32::createCMD(robotarm::Joints& move) {
    stringstream cmd;
    int16_t pwm;
    if (move.base != move.UNSET) {
        pwm = remap(move.base, move.BASE_MIN, move.BASE_MAX, Servos::BASE_MIN, Servos::BASE_MAX);
        cmd << "#0P" << pwm << MAX_SPEED;
    }
    if (move.shoulder != move.UNSET) {
        pwm = remap(move.shoulder, move.SHOULDER_MIN, 40, Servos::SHOULDER_MIN, Servos::SHOULDER_MAX);
        cmd << "#1P" << pwm << "S700";
    }
    if (move.elbow != move.UNSET) {
        pwm = remap(move.elbow, move.ELBOW_MIN, move.ELBOW_MAX, Servos::ELBOW_MIN, Servos::ELBOW_MAX);
        cmd << "#2P" << pwm << MAX_SPEED;
    }
    if (move.wrist != move.UNSET) {
        pwm = remap(move.wrist, move.WRIST_MIN, move.WRIST_MAX, Servos::WRIST_MIN, Servos::WRIST_MAX);
        cmd << "#3P" << pwm << MAX_SPEED;
    }
    if (move.wristRotation != move.UNSET) {
        pwm = remap(move.wristRotation, move.WRISTROTATION_MIN, move.WRISTROTATION_MAX, Servos::WRISTROTATION_MIN, Servos::WRISTROTATION_MAX);
        cmd << "#5P" << pwm << MAX_SPEED;
    }
    if (move.gripper != move.UNSET) {
        pwm = remap(move.gripper, move.GRIPPER_MIN, move.GRIPPER_MAX, Servos::GRIPPER_MIN, Servos::GRIPPER_MAX);
        cmd << "#4P" << pwm << MAX_SPEED;
    }
    if (move.time != 0) {
        cmd << "T" << move.time;
    }
    cmd << '\r';
    return cmd.str();
}

LowInterface::LowInterface(string name, const string& port, uint32_t baudrate)
: server(nh, name, false) {
    
#ifdef SERIAL
        if(Serial::getInstance().start(DEFAULT_PORT, BAUDRATE))
        {
            Serial::getInstance().write("#0PO100\r");
            Serial::getInstance().write("#4PO100\r");
        }  
#endif
    server.registerGoalCallback(boost::bind(&LowInterface::goalCB, this));
    server.registerPreemptCallback(boost::bind(&LowInterface::preemptCB, this));
    pub = nh.advertise<std_msgs::String>("SSC32Commands", 30);
    server.start();
}

LowInterface::~LowInterface() {
    Serial::getInstance().stop();
}

void LowInterface::spin() {
    // Just for testing
//    ssc32.jointsFromCMD(string("test"));
    
    ros::Rate r(REFRESH_RATE);
    int feebackRate = 0;
    while (ros::ok()) {
        if (server.isActive()) {
            if (ros::Time::now() > expectedEnd) {
                result.end = ros::Time::now();
                server.setSucceeded(result);
            }
            if (feebackRate % (REFRESH_RATE / FEEDBACK_RATE) == 0) {
                feedback.progress = ros::Time::now() - result.start;
                server.publishFeedback(feedback);
            }
        }
        feebackRate++;
        ros::spinOnce();
        r.sleep();
    }
}

void LowInterface::goalCB() {

    goal = *server.acceptNewGoal();
    // This needs to be called after acceptNewGoal because there is a possibility
    // to miss a preempt request between invoking this callback and accepting the goal.
    // Since the preempt callback runs on the same serial thread.
    if (server.isPreemptRequested()) {
        server.setPreempted();
    }
    ROS_INFO("New goal...");

    msg.data = ssc32.createCMD(goal.move);
    pub.publish(msg);
    Serial::getInstance().write(msg.data);

    float desiredDur = ((float) goal.move.time) / 1000;
    ros::Duration dur(desiredDur);
    result.start = ros::Time::now();
    expectedEnd = result.start + dur;
}

void LowInterface::preemptCB() {
    server.setPreempted();
    msg.data = ssc32.createStopCMD();
    pub.publish(msg);
    Serial::getInstance().write(msg.data);
    ROS_INFO("Goal got canceled");
}