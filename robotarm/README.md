## Robot Arm ##

### Install instructions ###
##### To run the project on your machine, execute the following commands in your terminal: #####

```
roscd
cd ..
catkin_make
roslaunch robotarm AL5D_Interface.launch

```

### UML Design ###

#### UML Use Case Diagram ####
![UseCase Diagram](docs/images/UseCaseDiagram.png "Use Case")

---
#### UML Component Diagram ####

![Component Diagram.png](docs/images/ComponentDiagram.png)

---
#### UML Sequence Diagram ####
![SequenceDiagramopstartinit](docs/images/SequenceDiagramopstartinit.png)

---
#### UML State Diagram LowInterface ####
![StateDiagramLowInterface.png](docs/images/StateDiagramLowInterface.png)

---
#### UML State Diagram ActionManager ####
![StateDiagramActionManager](docs/images/StateDiagramActionManager.png)

---
#### UML Time Diagram Gripper Movement ####
![TimingDiagram.jpg](docs/images/TimingDiagram.jpg)
---
#### Robot AL5D ####
![robot](docs/images/robot.jpg)

#### Niet te verhelpen warnings ####

```
warning: unused parameter ‘s’ [-Wunused-parameter] void exitHandler(int s)
```
Bovenstaande warning kan niet weggehaald worden, omdat dit een linux functie is die intern gebruik maakt van de parameter s. Zonder deze parameter compiled het programma niet.

```
warning: unused parameter ‘state’ [-Wunused-parameter] void ActionManager::actionDoneCallback(const actionlib::SimpleClientGoalState& state, const Result::ConstPtr& result)
```
Bovenstaande warning kan niet weggehaald worden, omdat ROS de state automatisch meestuurt. Deze state wordt verder niet gebruikt in de functie, waardoor deze warning ontstaat. 


