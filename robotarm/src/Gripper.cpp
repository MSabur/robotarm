#include <ros/ros.h>
#include <csignal>
#include <cmath>
#include <iostream>
#include <limits>
#include <cfloat>

#include "HighInterface.hpp"
#include "Kinematics.hpp"
#include "robotarm/PickAndPlace.h"

#define ALTITUDE_OFFSET 15;
#define OPEN_GRIPPER -90;
#define CLOSE_GRIPPER 90;

using std::cout;
using std::endl;
HighInterface *interface;

/**
 * Clean up the program, before it quit's and goes out of scope.
 **/
void exitHandler(int s)
{
    cout << endl;
    delete interface;
    ros::shutdown();
    exit(0);
}

/**
 * Enum with the states of the sequence.
 **/
enum InstructionStates
{
    MOVING_TO_PARK = 0,
    MOVING_TO_TARGET,
    LOWERING_TO_TARGET,
    PICKUP_TARGET,
    RAISING_FROM_TARGET,
    MOVING_TO_DEST,
    LOWERING_TO_DEST,
    RELEASE_TARGET_ON_DEST,
    RAISING_FROM_DEST
};

/**
 * This function checks if the target is reachable. The instruction is the X, Y, Z location of the target.
 * @input instruction The position where the target is.
 * @input targetRotation The rotation of the target.
 * @return returns the position of the joints if the position is reachable, else it returns invalid joint position.
 **/
robotarm::Joints inverseToInstruction(Point &instruction, float targetRotation)
{
    robotarm::Joints joints;
    joints.shoulder = 20;
    Kinematics kinematics;
    if (kinematics.inverse(instruction, joints))
    {
        cout << joints << endl;
        joints.wristRotation = joints.base;
        joints.wristRotation -= targetRotation;
        joints.wrist *= -1;
        joints.time = 2000;
        joints.gripper = -110;
    }
    else
    {
        cout << "not found!" << endl;
        joints.base = std::numeric_limits<double>::quiet_NaN();
        joints.shoulder = std::numeric_limits<double>::quiet_NaN();
        joints.elbow = std::numeric_limits<double>::quiet_NaN();
        joints.wrist = std::numeric_limits<double>::quiet_NaN();
        joints.gripper = std::numeric_limits<double>::quiet_NaN();
        joints.wristRotation = std::numeric_limits<double>::quiet_NaN();
        joints.time = std::numeric_limits<double>::quiet_NaN();
    }
    return joints;
}

/**
 * Check if there is no invalid value in the joints, if there is an invalid value then abort the instruction.
 * @input joints The joints given from the function inverseToInstruction
 * @see inverseToInstruction
 **/
bool jointIsReachable(robotarm::Joints &joints)
{
    if (!std::isnan(joints.base) && !std::isnan(joints.shoulder) && !std::isnan(joints.elbow) && !std::isnan(joints.wrist) && !std::isnan(joints.gripper) && !std::isnan(joints.wristRotation) && !std::isnan(joints.time))
        return true;
    return false;
}

/**
 * Callback for topic /DetectedShape with the message as argument.
 * This function handles the states in enum InstructionStates and operates the robot.
 * @param instruction The incoming message over the topic.
 * @see InstructionStates
 **/
void onShape(const robotarm::PickAndPlace::ConstPtr &instruction)
{
    InstructionStates instructionStates = MOVING_TO_TARGET;
    robotarm::Joints joints;
    bool instructionIsFinished = false;

    while (!instructionIsFinished)
    {
        switch (instructionStates)
        {
            case MOVING_TO_PARK:
            {
                interface->setStance("park");
                interface->publishMove();
                instructionIsFinished = true;
                break;
            }
            case MOVING_TO_TARGET:
            {
                Point p;
                p.x = instruction->target.x;
                p.y = instruction->target.y;
                p.z = instruction->target.z;
                joints = inverseToInstruction(p, instruction->targetRotation);
                if (jointIsReachable(joints))
                {
                    joints.shoulder -= ALTITUDE_OFFSET;
                    joints.gripper = OPEN_GRIPPER;
                    interface->publishMove(joints);
                    instructionStates = LOWERING_TO_TARGET;
                }
                else
                {
                    std::cout << "Instruction isn't reachable. Breaking the operation." << std::endl;
                    instructionStates = MOVING_TO_PARK;
                }
                break;
            }
            case LOWERING_TO_TARGET:
            {
                joints.shoulder += ALTITUDE_OFFSET;
                interface->publishMove(joints);
                instructionStates = PICKUP_TARGET;
                break;
            }
            case PICKUP_TARGET:
            {
                joints.gripper = CLOSE_GRIPPER;
                interface->publishMove(joints);
                instructionStates = RAISING_FROM_TARGET;
                break;
            }
            case RAISING_FROM_TARGET:
            {
                joints.shoulder -= ALTITUDE_OFFSET;
                interface->publishMove(joints);
                instructionStates = MOVING_TO_DEST;
                break;
            }
            case MOVING_TO_DEST:
            {
                Point p;
                p.x = instruction->dest.x;
                p.y = instruction->dest.y;
                p.z = instruction->dest.z;
                joints = inverseToInstruction(p, instruction->destRotation);
                if (jointIsReachable(joints))
                {
                    joints.shoulder -= ALTITUDE_OFFSET;
                    joints.gripper = CLOSE_GRIPPER;
                    interface->publishMove(joints);
                    instructionStates = LOWERING_TO_DEST;
                }
                else
                {
                    std::cout << "Instruction isn't reachable. Breaking the operation." << std::endl;
                    instructionStates = MOVING_TO_PARK;
                }
                break;
            }
            case LOWERING_TO_DEST:
            {
                joints.shoulder += 5;
                interface->publishMove(joints);
                instructionStates = RELEASE_TARGET_ON_DEST;
                break;
            }
            case RELEASE_TARGET_ON_DEST:
            {
                joints.gripper = OPEN_GRIPPER;
                interface->publishMove(joints);
                instructionStates = RAISING_FROM_DEST;
                break;
            }
            case RAISING_FROM_DEST:
            {
                joints.shoulder -= ALTITUDE_OFFSET;
                interface->publishMove(joints);
                instructionStates = MOVING_TO_PARK;
                break;
            }
            default:
            {
                std::cout << "You shouldn't be here" << std::endl;
                instructionStates = MOVING_TO_PARK;
                break;
            }
        }
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Gripper", ros::init_options::NoSigintHandler);
    ros::NodeHandle nh;

    interface = new HighInterface;

    signal(SIGINT, exitHandler);
    interface->waitForSubs();

    ROS_INFO("Sending initial stance...");
    interface->setStance("park");
    interface->publishMove();

    ros::Subscriber subsc = nh.subscribe("DetectedShape", 20, onShape);

    ros::spin();
    return 0;
}
