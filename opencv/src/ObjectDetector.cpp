/*
 ro * ObjectDetector.cpp
 *
 *  Created on: Feb 24, 2017
 *      Author: maurice
 * 		Email: 	m.rozema2@student.han.nl
 */

#include "ObjectDetector.hpp"

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/core/operations.hpp>
#include <opencv2/core/types_c.h>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/types_c.h>
#include <stddef.h>
#include <string>
#include <unistd.h>
#include <utility>

ObjectDetector::ObjectDetector()
    : e_program_state(WAIT_FOR_INPUT), serial_parser(), video_thread() {
  cv::Mat tmp;
  tmp = video_thread.getWebcamFrame();
  result_window = cv::Mat(cv::Size(tmp.cols, tmp.rows + 50), CV_8UC3);
  cv::namedWindow("result", CV_WINDOW_AUTOSIZE);
  cv::namedWindow("TestShow", CV_WINDOW_AUTOSIZE);
  std::thread([this]() { video_thread.video_capture(); }).detach();
  std::thread([this]() {
    serial_parser.waitForUserInput(object_specifications);
  }).detach();
  programState();
}

ObjectDetector::ObjectDetector(const std::string &pathToBashFile)
    : e_program_state(WAIT_FOR_INPUT), serial_parser(), video_thread(),
      bash_file(pathToBashFile) {
  std::thread([this]() { video_thread.video_capture(); }).detach();
  std::thread([this]() {
    serial_parser.BashInput(bash_file, object_specifications);
  }).detach();
  programState();
}

ObjectDetector::~ObjectDetector() {
  // TODO Auto-generated destructor stub
}

void ObjectDetector::programState() {
  while (true) {
    switch (e_program_state) {
    case CONTOUR_TO_TEXT: {
      contourToText();
      e_program_state = CLEAN_ALL;
      break;
    }
    case CLEAN_ALL: {
      right_contours.clear();
      circles.clear();
      beginClock = 0;
      endClock = 0;

      e_program_state = WAIT_FOR_INPUT;
      break;
    }
    case DRAWSHAPES: {
      drawShapes(origin_image, right_contours, circles);
      e_program_state = CONTOUR_TO_TEXT;
      break;
    }
    case SEND_OBJECT: {
      if (right_contours.size() > 0)
        sender.sendMessage(message);
      else
        std::cout << "Geen vierkant of cirkel gevonden" << std::endl;
      e_program_state = DRAWSHAPES;
      break;
    }
    case CALCULATE_DIFFERENCE: {
      if (right_contours.size() > 0)
        message = calculateDifference(right_contours[0], resultCircle);
      else if (circles.size() > 0)
        message =
            calculateDifference(circles[0], resultCircle); // 0 is the circle
      else
        std::cout
            << "You found a weird shape. Congratulations you broke the program."
            << std::endl;
      e_program_state = SEND_OBJECT;
      break;
    }
    case DETECTING: {
      std::cout << "CurObject: " << current_object.first << std::endl;
      detectShapes(current_object.first, color_image);
      if (bash_file.empty())
        e_program_state = CALCULATE_DIFFERENCE;
      else if (!bash_file.empty())
        e_program_state = CONTOUR_TO_TEXT;
      else
        std::cout << "MUCH ERROR?" << std::endl;
      break;
    }
    case TRESHHOLD: {
      origin_image = video_thread.getWebcamFrame();
      cv::cvtColor(origin_image, color_image, CV_BGR2HSV);
      basicColorTreshold(color_image, current_object.second);
      detectColors(current_object.second, color_image);
      basicGrayTreshold(color_image);
      // Invert colors, better for shape detection.
      cv::bitwise_not(color_image, color_image);
      cv::imshow("TestShow", color_image);
      e_program_state = DETECTING;
      break;
    }
    case DETECTCIRCLE: {
      cv::Mat tmpMat = origin_image.clone();
      cvtColor(tmpMat, tmpMat, CV_BGR2GRAY);
      GaussianBlur(tmpMat, tmpMat, cv::Size(9, 9), 2, 2);
      detectShapes(object_shapes::cirkel, tmpMat);
      if (savePositionResult()) {
        e_program_state = TRESHHOLD;
        std::cout << "Position: " << resultCircle << std::endl;
      } else {
        e_program_state = CLEAN_ALL;
        std::cout << "Cleaning: " << std::endl;
      }
      break;
    }
    case WAIT_FOR_INPUT: {
      if (object_specifications.size() > 0) {
        origin_image = video_thread.getWebcamFrame();
        zeroPoint = cv::Vec3f(0.0, float((origin_image.rows / 2)), 0.0);
        current_object = object_specifications[0];
        object_specifications.erase(object_specifications.begin());
        beginClock = clock();
        e_program_state = DETECTCIRCLE;
      } else
        sleep(2);
      break;
    }
    default: { break; }
      cv::waitKey(50);
    }
  }
}

void ObjectDetector::detectColors(object_colors e_object_color,
                                  cv::Mat &color_mat) {
  if (e_object_color == rood) {
    cv::Mat upperRed, lowerRed;
    cv::inRange(color_mat, cv::Scalar(0, 60, 50), cv::Scalar(10, 255, 255),
                lowerRed);
    cv::inRange(color_mat, cv::Scalar(160, 100, 100), cv::Scalar(180, 255, 255),
                upperRed);
    cv::addWeighted(lowerRed, 1.0, upperRed, 1.0, 0.0, color_mat);
  } else if (e_object_color == groen)
    cv::inRange(color_mat, cv::Scalar(40, 60, 60), cv::Scalar(90, 255, 255),
                color_mat);
  else if (e_object_color == blauw)
    cv::inRange(color_mat, cv::Scalar(90, 50, 50), cv::Scalar(130, 255, 255),
                color_mat);
  else if (e_object_color == geel)
    cv::inRange(color_mat, cv::Scalar(17, 60, 60), cv::Scalar(35, 255, 255),
                color_mat);
  else if (e_object_color == zwart)
    cv::inRange(color_mat, cv::Scalar(0, 0, 0), cv::Scalar(180, 255, 30),
                color_mat);
  else if (e_object_color == wit)
    cv::inRange(color_mat, cv::Scalar(0, 0, 160), cv::Scalar(180, 50, 255),
                color_mat);
}

void ObjectDetector::detectShapes(object_shapes e_object_shape,
                                  const cv::Mat &color_mat) {
  cv::Mat circles_mat = color_mat.clone();
  std::vector<std::vector<cv::Point>> contours;

  if (e_object_shape != cirkel) {
    cv::findContours(color_mat, contours, CV_RETR_EXTERNAL,
                     CV_CHAIN_APPROX_SIMPLE);
  } else if (e_object_shape == cirkel) {
    cv::HoughCircles(circles_mat, circles, CV_HOUGH_GRADIENT, 1, 30, 100, 30);
    // cv::HoughCircles( color_mat, circles, CV_HOUGH_GRADIENT, 2,
    // color_mat.rows/8, 200, 100, 0, 0 );
    std::cout << "CirclesSize: " << circles.size() << std::endl;
  }

  endClock = clock();

  if (contours.size() > 0) {
    std::vector<cv::Point> approx;
    std::vector<cv::Rect> boundRect(contours.size());

    for (ushort i = 0; i < contours.size(); i++) {
      cv::approxPolyDP(cv::Mat(contours[i]), approx,
                       cv::arcLength(cv::Mat(contours[i]), true) * 0.04, true);

      if (std::fabs(cv::contourArea(contours[i])) < 100 ||
          !cv::isContourConvex(approx))
        continue;

      boundRect[i] = cv::boundingRect(approx);

      switch (e_object_shape) {
      case driehoek:
        if (approx.size() == 3)
          right_contours.push_back(contours[i]);
        break;
      case vierkant:
        if (approx.size() == 4 && isSquare(contours[i])) {
          right_contours.push_back(contours[i]);
        }
        break;
      case rechthoek:
        if (approx.size() == 4 && !isSquare(contours[i]) &&
            isRectangle(contours[i])) {
          right_contours.push_back(contours[i]);
        }
        break;
      case halve_cirkel: {
        if (approx.size() >= 5 && approx.size() <= 7 &&
            isHalfCircle(contours[i])) {
          right_contours.push_back(contours[i]);
        }
        break;
      }
      default:
        break;
      }
    }
  }

  if ((right_contours.size() == 0) && (circles.size() == 0)) {
    std::cout << "Ticks: " << (endClock - beginClock) << std::endl;
    std::cout << "No " << e_object_shape << " found." << std::endl;
  }
}

void ObjectDetector::removeShadowEffects(cv::Mat &aMat, object_colors aColor) {
  if (aColor == wit || aColor == zwart)
    return;
  for (int i = 0; i < aMat.rows; i++) {
    for (int j = 0; j < aMat.cols; j++) {
      if (aMat.at<cv::Vec3b>(i, j)[1] <= 50 ||
          aMat.at<cv::Vec3b>(i, j)[2] <= 50) {
        aMat.at<cv::Vec3b>(i, j)[0] = 0;
        aMat.at<cv::Vec3b>(i, j)[1] = 0;
        aMat.at<cv::Vec3b>(i, j)[2] = 255;
      }
    }
  }
}

void ObjectDetector::basicGrayTreshold(cv::Mat &aMat) {
  cv::erode(aMat, aMat, cv::Mat());
  cv::adaptiveThreshold(aMat, aMat, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C,
                        CV_THRESH_BINARY, 11, 2);
  cv::medianBlur(aMat, aMat, 5);
}

void ObjectDetector::basicColorTreshold(cv::Mat &aMat, object_colors aColor) {
  removeShadowEffects(aMat, aColor);
  cv::pyrMeanShiftFiltering(aMat, aMat, 10, 50);
}

bool ObjectDetector::isSquare(const cv::vector<cv::Point> &contour) {
  // return ((aRect.height - aRect.width) <= 10 &&
  //         (aRect.height - aRect.width >= 0)) ||
  //        ((aRect.width - aRect.height) <= 10 &&
  //         (aRect.width - aRect.height) >= 0);
  cv::RotatedRect rotRect = minAreaRect(contour);
  std::cout << "RotRect: " << rotRect.size.width << std::endl;
  std::cout << "RotRect: " << rotRect.size.height << std::endl;
  std::cout << "isSquare: " << rotRect.size.width / rotRect.size.height
            << std::endl;
  return (std::abs(rotRect.size.width / rotRect.size.height) > 0.9 &&
          std::abs(rotRect.size.width / rotRect.size.height) < 1.1);
}

bool ObjectDetector::isRectangle(const cv::vector<cv::Point> &contour) {
  cv::RotatedRect rotRect = minAreaRect(contour);
  double contourSurface = contourArea(contour);
  double rotRectSurface = rotRect.size.height * rotRect.size.width;

  std::cout << "isRectangle: " << rotRectSurface / contourSurface << std::endl;

  return rotRectSurface / contourSurface > 1.05;
}

bool ObjectDetector::isHalfCircle(const cv::vector<cv::Point> &contour) {
  double radius;
  cv::RotatedRect roundRect = cv::minAreaRect(contour);

  if (roundRect.size.width < roundRect.size.height)
    radius = roundRect.size.width;
  else
    radius = roundRect.size.height;

  double oppervlakte = roundRect.size.width * roundRect.size.height;
  double oppervlakteCircle = (M_PI * std::pow(radius, 2)) / 2;

  double isHalfCircle = oppervlakteCircle / oppervlakte;

  if (abs((isHalfCircle - (M_PI / 4))) <= 0.15)
    return true;

  return false;
}

void ObjectDetector::drawShapes(
    const cv::Mat &aMat, const std::vector<std::vector<cv::Point>> &aContour,
    const std::vector<cv::Vec3f> &aCircle) {
  double elapsedTime = double(endClock - beginClock);
  cv::Mat drawMat = aMat.clone();

  if (aContour.size() > 0) {
    for (ushort i = 0; i < aContour.size(); i++) {
      auto M = cv::moments(aContour[i]);
      double cX = M.m10 / M.m00;
      double cY = M.m01 / M.m00;
      cv::drawContours(drawMat, aContour, i, cv::Scalar(0, 0, 255));
      cv::circle(drawMat, cv::Point((int)cX, (int)cY), 3,
                 cv::Scalar(255, 0, 0));
      double perimeter = cv::contourArea(aContour[i], true);
      cv::putText(drawMat, std::to_string(std::abs(int(perimeter))),
                  aContour[i][0], 1, 1, cv::Scalar(0xFF, 0xFF, 0xFF));
    }
  } else if (aCircle.size() > 0) {
    for (size_t i = 0; i < aCircle.size(); i++) {
      cv::Point center(cvRound(aCircle[i][0]), cvRound(aCircle[i][1]));
      int radius = cvRound(aCircle[i][2]);
      cv::circle(drawMat, center, radius, cv::Scalar(255, 0, 0), 3, 8, 0);
      cv::circle(drawMat, center, 3, cv::Scalar(0, 255, 0), -1, 8, 0);
      double perimeter = M_PI * floor(std::pow(radius, 2));
      cv::putText(drawMat, std::to_string(int(perimeter)), center, 1, 1,
                  cv::Scalar(0xFF, 0xFF, 0xFF));
    }
  }

  cv::Mat timeMat(cv::Size(drawMat.cols, 50), CV_8UC3,
                  cv::Scalar(255, 255, 255));
  cv::putText(timeMat, "Time: " + boost::lexical_cast<std::string>(elapsedTime),
              cv::Point2f(0, 35), 1, 1, cv::Scalar(0, 0, 0));

  drawMat.copyTo(result_window(cv::Rect(0, 0, drawMat.cols, drawMat.rows)));

  timeMat.copyTo(
      result_window(cv::Rect(0, drawMat.rows, timeMat.cols, timeMat.rows)));

  cv::imshow("result", result_window);
}

void ObjectDetector::contourToText() {
  if (right_contours.size() > 0 || circles.size() > 0) {
    std::cout << "==================" << std::endl;
    std::cout << "Object: " << current_object.first << std::endl;
    std::cout << "Kleur: " << current_object.second << std::endl;
    std::cout << "Ticks: " << (endClock - beginClock) << std::endl;
    std::cout << "====" << std::endl;
    for (ushort i = 0; i < right_contours.size(); i++) {
      double perimeter = cv::arcLength(right_contours[i], true);
      std::cout << "Object: " << (i + 1) << std::endl;
      std::cout << "X: " << right_contours[i][0].x << std::endl;
      std::cout << "Y: " << right_contours[i][0].y << std::endl;
      std::cout << "Omtrek: " << perimeter << std::endl;
      std::cout << "====" << std::endl;
    }

    for (ushort i = 0; i < circles.size(); i++) {
      cv::Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
      int radius = cvRound(circles[i][2]);
      std::cout << "X: " << center.x << std::endl;
      std::cout << "Y: " << center.y << std::endl;
      std::cout << "Radius: " << radius << std::endl;
      std::cout << "Oppervlakte: " << M_PI * floor(std::pow(radius, 2))
                << std::endl;
    }
    std::cout << "==================" << std::endl;
  }
}

bool ObjectDetector::savePositionResult() {
  if (right_contours.size() > 0 || circles.size() > 0) {
    if (circles.size() > 1) {
      std::cout << "Found to many white circles, please try again."
                << std::endl;
      return false;
    }

    resultCircle = circles[0];
    circles.erase(circles.begin());
    return resultCircle[0] != 0;
  }
  return false;
}

robotarm::PickAndPlace ObjectDetector::calculateDifference(std::vector<cv::Point> aContourVec,
                                    cv::Vec3f aResultVec) {
  float pixelScale = ((DIAMETER_OF_CIRCLE * 10.0) / 2.0) / aResultVec[2];
  cv::RotatedRect rotRect = minAreaRect(aContourVec);

  //	//Choose which side to use, always use the smallest possible.
  //	float tmpVar;
  //	{
  //		if (rotRect.size.width > rotRect.size.height)
  //			tmpVar = rotRect.size.height;
  //		else tmpVar = rotRect.size.height;
  //	}

  // Get the center in pixels
  auto M = cv::moments(aContourVec);
  double cX = M.m10 / M.m00;
  double cY = M.m01 / M.m00;
  // Calculate the new Y from the zeropoint
  aResultVec[1] -= zeroPoint[1];
  cY -= zeroPoint[1];

  robotarm::PickAndPlace message;
  message.target.x = cY * pixelScale;
  message.target.y = (cX * pixelScale) + OFFSET_TO_ROBOTARM;
  message.target.z = OFFSET_TO_GROUND;
  if (rotRect.angle != 0)
    message.targetRotation = rotRect.angle;
  else if (rotRect.angle == 90)
    message.targetRotation = 0;
  else
    message.targetRotation = 90;

  message.dest.x = aResultVec[1] * pixelScale;
  message.dest.y = (aResultVec[0] * pixelScale) + OFFSET_TO_ROBOTARM;
  message.dest.z = OFFSET_TO_GROUND;

  std::cout << message << std::endl;

  return message;
}

robotarm::PickAndPlace ObjectDetector::calculateDifference(cv::Vec3f aTargetVec,
                                    cv::Vec3f aResultVec) {
  float pixelScale = ((DIAMETER_OF_CIRCLE * 10.0) / 2.0) / aResultVec[2];

  // Get the center in pixels
  double cX = cvRound(aTargetVec[0]);
  double cY = cvRound(aTargetVec[1]);
  // Calculate the new Y from the zeropoint
  aResultVec[1] -= zeroPoint[1];
  cY -= zeroPoint[1];

  robotarm::PickAndPlace message;
  message.target.x = cY * pixelScale;
  message.target.y = (cX * pixelScale) + OFFSET_TO_ROBOTARM;
  message.target.z = OFFSET_TO_GROUND;

  message.dest.x = aResultVec[1] * pixelScale;
  message.dest.y = (aResultVec[0] * pixelScale) + OFFSET_TO_ROBOTARM;
  message.dest.z = OFFSET_TO_GROUND;

  return message;
}


// /*
//  ro * ObjectDetector.cpp
//  *
//  *  Created on: Feb 24, 2017
//  *      Author: maurice
//  * 		Email: 	m.rozema2@student.han.nl
//  */

// #include "ObjectDetector.hpp"

// #include <iostream>
// #include <opencv2/core/core.hpp>
// #include <opencv2/core/mat.hpp>
// #include <opencv2/core/operations.hpp>
// #include <opencv2/core/types_c.h>
// #include <opencv2/highgui/highgui_c.h>
// #include <opencv2/imgproc/types_c.h>
// #include <stddef.h>
// #include <string>
// #include <unistd.h>
// #include <utility>

// ObjectDetector::ObjectDetector() :
// 		e_program_state(WAIT_FOR_INPUT), serial_parser(), video_thread() {
// 	cv::Mat tmp;
// 	tmp = video_thread.getWebcamFrame();
// 	result_window = cv::Mat(cv::Size(tmp.cols, tmp.rows + 50), CV_8UC3);
// 	cv::namedWindow("result", CV_WINDOW_AUTOSIZE);
// 	cv::namedWindow("TestShow", CV_WINDOW_AUTOSIZE);
// 	std::thread([this]() {video_thread.video_capture();}).detach();
// 	std::thread([this]() {
// 		serial_parser.waitForUserInput(object_specifications);
// 	}).detach();
// 	programState();
// }

// ObjectDetector::ObjectDetector(const std::string& pathToBashFile) :
// 		e_program_state(WAIT_FOR_INPUT), serial_parser(), video_thread(), bash_file(
// 				pathToBashFile) {
// 	std::thread([this]() {video_thread.video_capture();}).detach();
// 	std::thread([this]() {
// 		serial_parser.BashInput(bash_file, object_specifications);
// 	}).detach();
// 	programState();
// }

// ObjectDetector::~ObjectDetector() {
// 	// TODO Auto-generated destructor stub
// }

// void ObjectDetector::programState() {
// 	while (true) {
// 		switch (e_program_state) {
// 		case CONTOUR_TO_TEXT: {
// 			contourToText();
// 			e_program_state = CLEAN_ALL;
// 			break;
// 		}
// 		case CLEAN_ALL: {
// 			right_contours.clear();
// 			circles.clear();
// 			beginClock = 0;
// 			endClock = 0;

// 			e_program_state = WAIT_FOR_INPUT;
// 			break;
// 		}
// 		case DRAWSHAPES: {
// 			drawShapes(origin_image, right_contours, circles);
// 			e_program_state = CONTOUR_TO_TEXT;
// 			break;
// 		}
// 		case SEND_OBJECT: {
// 			if(right_contours.size() > 0 || circles.size() > 1)
// 				sender.sendMessage(message);
// 			else 
// 				std::cout << "Geen vierkant of cirkel gevonden" << std::endl;
// 			e_program_state = DRAWSHAPES;
// 			break;
// 		}
// 		case CALCULATE_DIFFERENCE: {
// 			if(right_contours.size() > 0)
// 				message = calculateDifference(right_contours[0], resultCircle);
// 			else if (circles.size() > 0)
// 				message = calculateDifference(circles[0], resultCircle); //0 is the circle

// 			e_program_state = SEND_OBJECT;
// 			break;
// 		}
// 		case DETECTING: {
// 			detectShapes(current_object.first, color_image);
// 			if (bash_file.empty())
// 				e_program_state = CALCULATE_DIFFERENCE;
// 			else if (!bash_file.empty())
// 				e_program_state = CONTOUR_TO_TEXT;
// 			else
// 				std::cout << "MUCH ERROR?" << std::endl;
// 			break;
// 		}
// 		case TRESHHOLD: {
// 			origin_image = video_thread.getWebcamFrame();
// 			cv::cvtColor(origin_image, color_image, CV_BGR2HSV);
// 			basicColorTreshold(color_image, current_object.second);
// 			detectColors(current_object.second, color_image);
// 			basicGrayTreshold(color_image);
// 			// Invert colors, better for shape detection.
// 			cv::bitwise_not(color_image, color_image);
// 			cv::imshow("TestShow", color_image);
// 			e_program_state = DETECTING;
// 			break;
// 		}
// 		case DETECTCIRCLE: {
// 			cv::Mat tmpMat = origin_image.clone();
// 			cvtColor( tmpMat, tmpMat, CV_BGR2GRAY );
// 			GaussianBlur( tmpMat, tmpMat, cv::Size(9, 9), 2, 2 );
// 			detectShapes(object_shapes::cirkel, tmpMat);
// 			if (savePositionResult()) {
// 				e_program_state = TRESHHOLD;
// 			} else {
// 				e_program_state = CLEAN_ALL;
// 			}
// 			break;
// 		}
// 		case WAIT_FOR_INPUT: {
// 			if (object_specifications.size() > 0) {
// 				origin_image = video_thread.getWebcamFrame();
// 				zeroPoint = cv::Vec3f(0.0, float((origin_image.rows / 2)), 0.0);
// 				current_object = object_specifications[0];
// 				object_specifications.erase(object_specifications.begin());
// 				beginClock = clock();
// 				e_program_state = DETECTCIRCLE;
// 			} else
// 				sleep(2);
// 			break;
// 		}
// 		default: {
// 			break;
// 		}
// 			cv::waitKey(50);
// 		}
// 	}
// }

// void ObjectDetector::detectColors(object_colors e_object_color,
// 		cv::Mat& color_mat) {
// 	if (e_object_color == rood) {
// 		cv::Mat upperRed, lowerRed;
// 		cv::inRange(color_mat, cv::Scalar(0, 60, 50), cv::Scalar(10, 255, 255),
// 				lowerRed);
// 		cv::inRange(color_mat, cv::Scalar(160, 100, 100),
// 				cv::Scalar(180, 255, 255), upperRed);
// 		cv::addWeighted(lowerRed, 1.0, upperRed, 1.0, 0.0, color_mat);
// 	} else if (e_object_color == groen)
// 		cv::inRange(color_mat, cv::Scalar(40, 60, 60), cv::Scalar(90, 255, 255),
// 				color_mat);
// 	else if (e_object_color == blauw)
// 		cv::inRange(color_mat, cv::Scalar(90, 50, 50),
// 				cv::Scalar(130, 255, 255), color_mat);
// 	else if (e_object_color == geel)
// 		cv::inRange(color_mat, cv::Scalar(17, 60, 60), cv::Scalar(35, 255, 255),
// 				color_mat);
// 	else if (e_object_color == zwart)
// 		cv::inRange(color_mat, cv::Scalar(0, 0, 0), cv::Scalar(180, 255, 30),
// 				color_mat);
// 	else if (e_object_color == wit)
// 		cv::inRange(color_mat, cv::Scalar(0, 0, 160), cv::Scalar(180, 50, 255),
// 				color_mat);
// }

// void ObjectDetector::detectShapes(object_shapes e_object_shape,
// 		const cv::Mat& color_mat) {
// 	cv::Mat circles_mat = color_mat.clone();
// 	std::vector<std::vector<cv::Point>> contours;

// 	if (e_object_shape != cirkel) {
// 		cv::findContours(color_mat, contours, CV_RETR_EXTERNAL,
// 				CV_CHAIN_APPROX_SIMPLE);
// 	} else if (e_object_shape == cirkel) {
// 		cv::HoughCircles(circles_mat, circles, CV_HOUGH_GRADIENT, 1, 30, 100, 30);
// 		//cv::HoughCircles( color_mat, circles, CV_HOUGH_GRADIENT, 2, color_mat.rows/8, 200, 100, 0, 0 );
// 	}

// 	endClock = clock();

// 	if (contours.size() > 0) {
// 		std::vector<cv::Point> approx;
// 		std::vector<cv::Rect> boundRect(contours.size());

// 		for (ushort i = 0; i < contours.size(); i++) {
// 			cv::approxPolyDP(cv::Mat(contours[i]), approx,
// 					cv::arcLength(cv::Mat(contours[i]), true) * 0.04, true);

// 			if (std::fabs(cv::contourArea(contours[i])) < 100
// 					|| !cv::isContourConvex(approx))
// 				continue;

// 			boundRect[i] = cv::boundingRect(approx);

// 			switch (e_object_shape) {
// 			case driehoek:
// 				if (approx.size() == 3)
// 					right_contours.push_back(contours[i]);
// 				break;
// 			case vierkant:
// 				if (approx.size() == 4 && isSquare(contours[i])) {
// 					right_contours.push_back(contours[i]);
// 				}
// 				break;
// 			case rechthoek:
// 				if (approx.size() == 4 && !isSquare(contours[i])
// 						&& isRectangle(contours[i])) {
// 					right_contours.push_back(contours[i]);
// 				}
// 				break;
// 			case halve_cirkel: {
// 				if (approx.size() >= 5 && approx.size() <= 7
// 						&& isHalfCircle(contours[i])) {
// 					right_contours.push_back(contours[i]);
// 				}
// 				break;
// 			}
// 			default:
// 				break;
// 			}
// 		}
// 	}

// 	if ((right_contours.size() == 0) && (circles.size() == 0)) {
// 		std::cout << "Ticks: " << (endClock - beginClock) << std::endl;
// 		std::cout << "No " << e_object_shape << " found." << std::endl;
// 	}
// }

// void ObjectDetector::removeShadowEffects(cv::Mat& aMat, object_colors aColor) {
// 	if (aColor == wit || aColor == zwart)
// 		return;
// 	for (int i = 0; i < aMat.rows; i++) {
// 		for (int j = 0; j < aMat.cols; j++) {
// 			if (aMat.at<cv::Vec3b>(i, j)[1] <= 50
// 					|| aMat.at<cv::Vec3b>(i, j)[2] <= 50) {
// 				aMat.at<cv::Vec3b>(i, j)[0] = 0;
// 				aMat.at<cv::Vec3b>(i, j)[1] = 0;
// 				aMat.at<cv::Vec3b>(i, j)[2] = 255;
// 			}
// 		}
// 	}
// }

// void ObjectDetector::basicGrayTreshold(cv::Mat& aMat) {
// 	cv::erode(aMat, aMat, cv::Mat());
// 	cv::adaptiveThreshold(aMat, aMat, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C,
// 			CV_THRESH_BINARY, 11, 2);
// 	cv::medianBlur(aMat, aMat, 5);
// }

// void ObjectDetector::basicColorTreshold(cv::Mat& aMat, object_colors aColor) {
// 	removeShadowEffects(aMat, aColor);
// 	cv::pyrMeanShiftFiltering(aMat, aMat, 10, 50);
// }

// bool ObjectDetector::isSquare(const cv::vector<cv::Point>& contour) {
// 	cv::RotatedRect rotRect = minAreaRect(contour);
// 	return (std::abs(rotRect.size.height / rotRect.size.width) > 0.9 && std::abs(rotRect.size.height / rotRect.size.width) < 1.1);
// }

// bool ObjectDetector::isRectangle(const cv::vector<cv::Point>& contour) {
// 	cv::RotatedRect rotRect = minAreaRect(contour);
// 	double contourSurface = contourArea(contour);
// 	double rotRectSurface = rotRect.size.height * rotRect.size.width;
// 	return (rotRectSurface / contourSurface > 1.15);
// }

// bool ObjectDetector::isHalfCircle(const cv::vector<cv::Point>& contour) {
// 	double radius;
// 	cv::RotatedRect roundRect = cv::minAreaRect(contour);

// 	if (roundRect.size.width < roundRect.size.height)
// 		radius = roundRect.size.width;
// 	else
// 		radius = roundRect.size.height;

// 	double oppervlakte = roundRect.size.width * roundRect.size.height;
// 	double oppervlakteCircle = (M_PI * std::pow(radius, 2)) / 2;

// 	double isHalfCircle = oppervlakteCircle / oppervlakte;

// 	if (abs((isHalfCircle - (M_PI / 4))) <= 0.15)
// 		return true;

// 	return false;
// }

// void ObjectDetector::drawShapes(const cv::Mat& aMat,
// 		const std::vector<std::vector<cv::Point>>& aContour,
// 		const std::vector<cv::Vec3f>& aCircle) {
// 	double elapsedTime = double(endClock - beginClock);
// 	cv::Mat drawMat = aMat.clone();

// 	if (aContour.size() > 0) {
// 		for (ushort i = 0; i < aContour.size(); i++) {
// 			auto M = cv::moments(aContour[i]);
// 			double cX = M.m10 / M.m00;
// 			double cY = M.m01 / M.m00;
// 			cv::drawContours(drawMat, aContour, i, cv::Scalar(0, 0, 255));
// 			cv::circle(drawMat, cv::Point((int) cX, (int) cY), 3,
// 					cv::Scalar(255, 0, 0));
// 			double perimeter = cv::contourArea(aContour[i], true);
// 			cv::putText(drawMat, std::to_string(std::abs(int(perimeter))),
// 					aContour[i][0], 1, 1, cv::Scalar(0xFF, 0xFF, 0xFF));
// 		}
// 	} else if (aCircle.size() > 0) {
// 		for (size_t i = 0; i < aCircle.size(); i++) {
// 			cv::Point center(cvRound(aCircle[i][0]), cvRound(aCircle[i][1]));
// 			int radius = cvRound(aCircle[i][2]);
// 			cv::circle(drawMat, center, radius, cv::Scalar(255, 0, 0), 3, 8, 0);
// 			cv::circle(drawMat, center, 3, cv::Scalar(0, 255, 0), -1, 8, 0);
// 			double perimeter = M_PI * floor(std::pow(radius, 2));
// 			cv::putText(drawMat, std::to_string(int(perimeter)), center, 1, 1,
// 					cv::Scalar(0xFF, 0xFF, 0xFF));
// 		}
// 	}

// 	cv::Mat timeMat(cv::Size(drawMat.cols, 50), CV_8UC3,
// 			cv::Scalar(255, 255, 255));
// 	cv::putText(timeMat,
// 			"Time: " + boost::lexical_cast<std::string>(elapsedTime),
// 			cv::Point2f(0, 35), 1, 1, cv::Scalar(0, 0, 0));

// 	drawMat.copyTo(result_window(cv::Rect(0, 0, drawMat.cols, drawMat.rows)));

// 	timeMat.copyTo(
// 			result_window(
// 					cv::Rect(0, drawMat.rows, timeMat.cols, timeMat.rows)));

// 	cv::imshow("result", result_window);
// }

// void ObjectDetector::contourToText() {
// 	if (right_contours.size() > 0 || circles.size() > 0) {
// 		std::cout << "==================" << std::endl;
// 		std::cout << "Object: " << current_object.first << std::endl;
// 		std::cout << "Kleur: " << current_object.second << std::endl;
// 		std::cout << "Ticks: " << (endClock - beginClock) << std::endl;
// 		std::cout << "====" << std::endl;
// 		for (ushort i = 0; i < right_contours.size(); i++) {
// 			double perimeter = cv::arcLength(right_contours[i], true);
// 			std::cout << "Object: " << (i + 1) << std::endl;
// 			std::cout << "X: " << right_contours[i][0].x << std::endl;
// 			std::cout << "Y: " << right_contours[i][0].y << std::endl;
// 			std::cout << "Omtrek: " << perimeter << std::endl;
// 			std::cout << "====" << std::endl;
// 		}

// 		for (ushort i = 0; i < circles.size(); i++) {
// 			cv::Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
// 			int radius = cvRound(circles[i][2]);
// 			std::cout << "X: " << center.x << std::endl;
// 			std::cout << "Y: " << center.y << std::endl;
// 			std::cout << "Radius: " << radius << std::endl;
// 			std::cout << "Oppervlakte: " << M_PI * floor(std::pow(radius, 2))
// 					<< std::endl;
// 		}
// 		std::cout << "==================" << std::endl;
// 	}
// }

// bool ObjectDetector::savePositionResult() {
// 	if (right_contours.size() > 0 || circles.size() > 0) {
// 		if (circles.size() > 1) {
// 			std::cout << "Found to many white circles, please try again."
// 					<< std::endl;
// 			return false;
// 		}

// 		resultCircle = circles[0];
// 		circles.erase(circles.begin());
// 		return resultCircle[0] != 0;
// 	}
// 	return false;
// }

// robotarm::PickAndPlace ObjectDetector::calculateDifference(
// 		std::vector<cv::Point> aContourVec, cv::Vec3f aResultVec) {
// 	float pixelScale = ((DIAMETER_OF_CIRCLE * 10.0) / 2.0) / aResultVec[2];
// 	cv::RotatedRect rotRect = minAreaRect(aContourVec);

// 	//Get the center in pixels
// 	auto M = cv::moments(aContourVec);
// 	double cX = M.m10 / M.m00;
// 	double cY = M.m01 / M.m00;
// 	//Calculate the new Y from the zeropoint
// 	aResultVec[1] -= zeroPoint[1];
// 	cY -= zeroPoint[1];

// 	robotarm::PickAndPlace message;
// 	message.target.x = cY * pixelScale;
// 	message.target.y = (cX * pixelScale) + OFFSET_TO_ROBOTARM;
// 	message.target.z = OFFSET_TO_GROUND;
// 	if(rotRect.angle != 0)
// 		message.targetRotation = rotRect.angle;
// 	else if (rotRect.angle == 90)
// 		message.targetRotation = 0;
// 	else
// 		message.targetRotation = 90;	

// 	message.dest.x = aResultVec[1] * pixelScale;
// 	message.dest.y = (aResultVec[0] * pixelScale) + OFFSET_TO_ROBOTARM;
// 	message.dest.z = OFFSET_TO_GROUND;

// 	std::cout << message << std::endl;

// 	return message;
// }

// robotarm::PickAndPlace ObjectDetector::calculateDifference(cv::Vec3f aTargetVec, cv::Vec3f aResultVec) {
// 	float pixelScale = ((DIAMETER_OF_CIRCLE * 10.0) / 2.0) / aResultVec[2];

// 	//Get the center in pixels
// 	double cX = cvRound(aTargetVec[0]);
// 	double cY = cvRound(aTargetVec[1]);
// 	//Calculate the new Y from the zeropoint
// 	aResultVec[1] -= zeroPoint[1];
// 	cY -= zeroPoint[1];

// 	robotarm::PickAndPlace message;
// 	message.target.x = cY * pixelScale;
// 	message.target.y = (cX * pixelScale) + OFFSET_TO_ROBOTARM;
// 	message.target.z = OFFSET_TO_GROUND;	

// 	message.dest.x = aResultVec[1] * pixelScale;
// 	message.dest.y = (aResultVec[0] * pixelScale) + OFFSET_TO_ROBOTARM;
// 	message.dest.z = OFFSET_TO_GROUND;

// 	return message;
// }
