/*
* Sender.hpp
*
*  Created on: Sep 27, 2017
*      Author: maurice
*/

#ifndef SRC_SENDER_HPP_
#define SRC_SENDER_HPP_

#include "ros/ros.h"
#include "robotarm/PickAndPlace.h"

class Sender {
public:
	/**
     * Constructor of the Sender class.
     * This class will send the custom ros message with the distance over a topic.
     **/
	Sender();

	/**
     * Destructor
     **/
	virtual ~Sender();

	/**
	 * This function send the ObjectTransform message over a specified topic.
	 **/
	void sendMessage(robotarm::PickAndPlace&);

private:
	ros::NodeHandle nh; //< Nodehandler
	ros::Publisher chatter_pub; //< Publisher
};

#endif /* SRC_SENDER_HPP_ */
