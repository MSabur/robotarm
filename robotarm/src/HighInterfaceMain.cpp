#include <ros/ros.h>
#include <signal.h>
#include <fstream>
#include "HighInterface.hpp"

using namespace std;

void exitHandler(int s) {
    cout << endl;
    ros::shutdown();
    exit(0);
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "HighInterface", ros::init_options::NoSigintHandler);
    HighInterface interface;

    signal(SIGINT, exitHandler);

    interface.waitForSubs();

    ROS_INFO("Sending initial stance...");
    interface.setStance("park");
    interface.publishMove();

    if (argc == 2) {
        ifstream file(argv[1]);
        if (file.is_open() && ros::ok()) {
            while (interface.getUserInput(file)) {
                if (interface.createMove(false)) {
                    interface.publishMove();
                }
                ros::spinOnce();
            }
            ROS_INFO("Valid movements are published");
        } else {
            ROS_INFO_STREAM("Unknown file path: " << argv[1]);
        }
    }

    while (ros::ok() && !interface.quit()) {
        if (interface.getUserInput(cin)) {
            if (interface.createInstaMsg()) {
                interface.publishInstaMsg();
            } else if (interface.createMove(false)) {
                interface.publishMove();

                ROS_INFO("Movement published!");
            }
        }
        ros::spinOnce();
    }

    ROS_INFO("Sending last stance...");
    interface.setStance("park");
    interface.publishMove();
    ros::spinOnce();
    ros::shutdown();

    return 0;
}
