/*
 * 
 */

#include "LowInterface.hpp"

int main(int argc, char** argv) {

    ros::init(argc, argv, "LowInterface");
    
    std::string port;
    
    if (argc == 2) {
      port =  argv[1];
    }
    else {
      port = DEFAULT_PORT;  
    }

    LowInterface lowInterface (ros::this_node::getName(), port);

    ROS_INFO("Start...");
    lowInterface.spin();

    return 0;
}
